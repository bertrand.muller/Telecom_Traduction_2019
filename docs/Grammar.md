# Grammaire

program		⇨ **program** IDF vardeclist* fundeclist* instr

varsuitdecl  	  ⇨ **var** identlist **:** typename **;**

​				⇨ | ε

identlist      	   ⇨ IDF ( **,** identlist )?

typename    	 ⇨ atomtype | arraytype

arraytype     	 ⇨ **array** **[** rangelist **]** **of** atomtype

rangelist       	 ⇨ CSTE **..** CSTE ( **,** rangelist )?

funcdecl      	  ⇨ **function** IDF **(** arglist **)** **:** atomtype vardeclist **{** sequence **}**

arglist          	  ⇨ arg ( **,** arglist )?

​			       ⇨ | ε

arg               	  ⇨ IDF **:** typename

​		      	 ⇨| **ref** IDF **:** typename

instr           	    ⇨ **if** expr1 **then** instr ( **else** instr )?
		   	    ⇨| **while** expr1 **do** instr
		   	    ⇨| **return** expr1?
		   	    ⇨| IDF instridf
		    	   ⇨ | **{** sequence? **}**
		    	   ⇨ | **read** lvalue
		    	   ⇨ | **write** ( lvalue|CSTE )

instridf		  ⇨  **(** exprlist? **)**
		    	   ⇨ | ( **[** exprlist **]** )? **=** expr1

sequence   	  ⇨ instr ( **;** sequence? )?

lvalue 		   ⇨ IDF ( **[** exprlist **]** )?

exprlist      	   ⇨ expr1 ( **,** exprlist )?

idfsuivant  	  ⇨ **(** exprlist? **)**
		   	   ⇨| **[** exprlist **]**
		   	   ⇨| ε

expr1		    ⇨ expr2 exp1Retour

exp1Retour         ⇨ ( **or** expr1 )?

expr2    	        ⇨ expr3 exp2Retour

exp2Retour         ⇨ ( **and** expr2 )?

expr3 	           ⇨ expr4 exp3Retour

exp3Retour         ⇨ ( ( **==** | **<=** | **>=** | **<** | **>** | **!=** ) expr3 )?

expr4                    ⇨ expr5 exp4Retour

exp4Retour         ⇨ ( ( **+** | **-** ) expr4 )?

expr5 	          ⇨ expr6 exp5Retour

exp5Retour        ⇨ ( ( **\*** | **/** ) expr5 )?

expr6	           ⇨ expr7

​		             ⇨| opun expr7

expr7 	          ⇨ expr8 exp7Retour

exp7Retour        ⇨ ( **^** expr7 )?

expr8 	          ⇨ CSTE | '(' expr1 ')' | IDF idfsuivant;

opun 		  ⇨ **-**
			    ⇨ | **not**

atomtype 	  ⇨ **void**
			    ⇨ | **bool**
			    ⇨ |**'int**
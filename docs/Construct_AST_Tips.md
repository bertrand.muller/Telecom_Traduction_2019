* Pour créer une relation entre un noeud virtuel et les éléments

  DecVar : 'var' ID ':' type

  ​	-> type ID

  ​	;

* Pour forcer un noeud de l'arbre abstrait :

  DecVar : 'var' ID ':' type

  ​	-> ^('DecVar' type ID)

  ​	;

* Exemples

  DecFunct: 'def' ID '(' listparam ')' ':' type '=' '{' body '}' 

  ​	-> ^('DecFunct' ID type listparam ^('Body' body))

  ​	;

  list: ID( ',' ID)*

  ​	-> ID+

  ​	;

  for_stat: 'for' '(' decl? ';' cond=expr? ';' iter=expr? ')' list_int

  ​	-> ^('FOR' decl? ^('COND' $cond)? ^('ITER' $iter)? list_int )

  ​	;

  Expr: leftV ':=' expr

  ​	-> ^('=' leftV expr)

  ​	;

  expr: INT -> INT ('+' i=INT -> ^('+' $expr $i))*

  ​	;

  expr: INT ( '+' ^INT )*

  ​	;












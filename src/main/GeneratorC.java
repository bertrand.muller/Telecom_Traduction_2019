package main;

import tds.Argument;
import tds.ArgumentArray;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class GeneratorC {

    private StringBuilder code;
    private boolean active;
    private Stack<String> functions;
    private ArrayList<String> refSymbols;
    private int indent;

    private HashMap<String, int[]> offset;

    public GeneratorC(){
        this.code = new StringBuilder();
        this.active = true;
        this.functions = new Stack<>();
        this.refSymbols = new ArrayList<>();
        this.indent = 0;
        this.offset = new HashMap<>();
    }

    private String generateIndent(int length){
        StringBuilder outputBuffer = new StringBuilder(length);
        for (int i = 0; i < length; i++){
            outputBuffer.append("    ");
        }
        return outputBuffer.toString();
    }

    public void initializeCode(){
        if(active){
            this.code.append("#include <stdlib.h>");
            this.code.append(System.lineSeparator());

            this.code.append("#include <stdio.h>");
            this.code.append(System.lineSeparator());

            this.code.append("#include <string.h>");
            this.code.append(System.lineSeparator());

            this.code.append("#include <stdbool.h>");
            this.code.append(System.lineSeparator());

            this.code.append("#include <math.h>");
            this.code.append(System.lineSeparator());

            /*this.code.append("int *arrayintcpy(int const *src, size_t len)");
            this.code.append(System.lineSeparator());
            this.code.append("{");
            this.code.append(System.lineSeparator());
            this.code.append("    int *p = malloc(len * sizeof(int));");
            this.code.append(System.lineSeparator());
            this.code.append("    memcpy(p, src, len * sizeof(int));");
            this.code.append(System.lineSeparator());
            this.code.append("    return p;");
            this.code.append(System.lineSeparator());
            this.code.append("}");
            this.code.append(System.lineSeparator());

            this.code.append("bool *arrayboolcpy(bool const *src, size_t len)");
            this.code.append(System.lineSeparator());
            this.code.append("{");
            this.code.append(System.lineSeparator());
            this.code.append("    bool *p = malloc(len * sizeof(bool));");
            this.code.append(System.lineSeparator());
            this.code.append("    memcpy(p, src, len * sizeof(bool));");
            this.code.append(System.lineSeparator());
            this.code.append("    return p;");
            this.code.append(System.lineSeparator());
            this.code.append("}");
            this.code.append(System.lineSeparator());*/
            this.code.append(System.lineSeparator());
        }
    }

    public void addPrimitiveVariable(String idf, String type){
        if(active) {
            this.code.append(generateIndent(this.indent));
            this.code.append(type);
            this.code.append(" ");
            this.code.append(idf);
            this.code.append(";");
            this.code.append(System.lineSeparator());
        }
    }

    public void addArrayVariable(String idf, String type, ArrayList<int[]> shape, String functionName){
        if(active){
            this.code.append(generateIndent(this.indent));
            this.code.append(type);
            this.code.append(" ");
            this.code.append(idf);
            for(int[] line : shape){
                this.code.append("[");
                this.code.append((line[1]-line[0])+1);
                this.code.append("]");
            }
            this.code.append(";");
            this.code.append(System.lineSeparator());
            // add offset
            addArrayOffset(functionName+"_"+idf, shape);
        }
    }

    public void addArrayOffset(String idf, ArrayList<int[]> shape){
        /*this.code.append(generateIndent(this.indent));
        this.code.append("int");
        this.code.append(" ");
        this.code.append(idf);
        this.code.append(OFFSET);
        this.code.append("[");
        this.code.append(shape.size());
        this.code.append("]");
        this.code.append(" = {");*/
        int[] array = new int[shape.size()];
        for(int i = 0; i<shape.size(); i++){
            array[i] = -shape.get(i)[0];
        }
        //this.code.append("};");
        //this.code.append(System.lineSeparator());
        this.offset.put(idf,array);
    }

    public void addFunction(String idf, String returnType, ArrayList<Argument> args){
        if(active){
            this.code.append(generateIndent(this.indent));
            this.code.append(returnType);
            this.code.append(" ");
            this.code.append(idf);
            this.code.append("(");
            for(Iterator<Argument> i = args.iterator(); i.hasNext();){
                Argument arg = i.next();
                switch(arg.getCat()){
                    case "primitive":
                        this.code.append(arg.getType());
                        this.code.append(" ");
                        if(arg.isRefmode()){
                            this.code.append("*");
                            this.refSymbols.add(arg.getIdf());
                        }
                        this.code.append(arg.getIdf());
                        break;
                    case "array":
                        this.code.append(arg.getType().split("\\[")[1].split("]")[0]);
                        this.code.append(" ");
                        this.code.append(arg.getIdf());
                        for(Iterator<int[]> l = arg.getShape().iterator(); l.hasNext();){
                            int[] line = l.next();
                            if(!l.hasNext()){
                                this.code.append("[");
                                this.code.append(line[1]-line[0]+1);
                                this.code.append("]");
                            } else {
                                this.code.append("[]");
                            }
                        }
                        break;
                    default:
                        //Nothing to do
                }
                if(i.hasNext()){
                    this.code.append(", ");
                }
            }
            this.code.append(")");
            this.code.append(System.lineSeparator());
            enterBloc();
            for(Argument argument : args){
                if(argument.getType().contains("array")){
                    addArrayOffset(idf+"_"+argument.getIdf(), ((ArgumentArray)argument).getShape());
                }
            }
        }
    }


    public void addAffectLeftMemberPrimitive(String idf){
        if(active) {
            this.code.append(generateIndent(this.indent));
            if(this.refSymbols.contains(idf)){
                this.code.append("*");
            }
            this.code.append(idf);
            this.code.append(" = ");
        }
    }

    private String arrayCode(String idf, String funcName, List<String> indexes){
        String res = idf;
        String index;
        for(int i=0; i<indexes.size() && i < this.offset.get(funcName+"_"+idf).length; i++){
            index = indexes.get(i);
            res += "[";
            res += index;
            res += "+("+this.offset.get(funcName+"_"+idf)[i];
            res += ")]";
        }
        return res;
    }

    public void addAffectLeftMemberArray(String idf, String funcName, List<String> indexes){
        if(active) {
            this.code.append(generateIndent(this.indent));
            this.code.append(arrayCode(idf, funcName, indexes));
            this.code.append(" = ");
        }
    }

    public void addIfBegin(){
        if(active){
            this.code.append(generateIndent(this.indent));
            this.code.append("if (");
        }
    }

    public void addElseBegin(){
        if(active){
            this.code.setLength(this.code.length() - 1);
            this.code.append(generateIndent(this.indent));
            this.code.append("else");
            this.code.append(System.lineSeparator());
        }
    }

    public void addWhileBegin(){
        if(active){
            this.code.append(generateIndent(this.indent));
            this.code.append("while (");
        }
    }

    public void endWhifInstruction(){
        if(active){
            this.code.append(")");
            this.code.append(System.lineSeparator());
        }
    }

    public void endGenericInstruction(){
        if(active){
            this.code.append(";");
            this.code.append(System.lineSeparator());
        }
    }

    public void addMainFunction(){
        if(active){
            this.code.append("int main()");
            this.code.append(System.lineSeparator());
        }
    }

    public void addCallFunc(String idf){
        if(active){
            this.functions.push(idf);
            int lastCR = this.code.lastIndexOf(System.lineSeparator());
            String lastLine = this.code.substring(lastCR, this.code.length());
            Pattern p = Pattern.compile("^\\s*$");
            if (p.matcher(lastLine).find()) {
                this.code.append(generateIndent(this.indent));
            }

            this.code.append(idf);
            this.code.append("(");
        }
    }
    public void endCallFunc(){
        if(active){
            this.code.append(")");

            int lastCR = this.code.lastIndexOf(System.lineSeparator());
            String lastLine = this.code.substring(lastCR, this.code.length());
            String idf = this.functions.pop();
            Pattern p = Pattern.compile("^\\s*"+idf);
            if (p.matcher(lastLine).find()) {
                this.code.append(";");
                this.code.append(System.lineSeparator());
            }
        }
    }

    public void addFuncArgumentArray(String name, String funcName, boolean refmode, String type, List<String> indexes){
        addFuncArgument(arrayCode(name, funcName, indexes), refmode, type);
    }

    public void addFuncArgument(String name, boolean refmode, String type){
        if(active) {
            if (type.contains("array")) {
                /*if (!refmode) {
                    String typeVal = type.split("\\[")[1].split("]")[0];
                    if (typeVal.equals("int")) {
                        this.code.append("arrayintcpy(");
                    } else {
                        this.code.append("arrayboolcpy(");
                    }
                    this.code.append(name);
                    this.code.append(")");
                } else {
                    this.code.append(name);
                }*/
                this.code.append(name);
            } else {
                if (refmode) {
                    this.code.append("&");
                }
                this.code.append(name);
            }
        }
    }

    public void addExprArray(String expr, String funcName, List<String> indexes){
        addExpr(arrayCode(expr, funcName, indexes));
    }

    public void addExpr(String expr){
        if(active){
            if(this.refSymbols.contains(expr)){
                this.code.append("*");
            }
            this.code.append(expr);
        }
    }

    public void addReturn() {
        if(active) {
            this.code.append(generateIndent(this.indent));
            this.code.append("return ");
        }
    }

    public void addRead(String idf, String funcName, String type, String... indexes) {
        if(active) {
            this.code.append(generateIndent(this.indent));
            this.code.append("scanf(");

            switch(type) {
                case "int":
                case "bool":
                    this.code.append("\"%d\", ");
                    if(!this.refSymbols.contains(idf)){
                        this.code.append("&");
                    }
                    this.code.append(idf);
                    break;
                case "ARRAY":
                    this.code.append("\"%d\", &");
                    this.code.append(arrayCode(idf, funcName, Arrays.stream(indexes).collect(Collectors.toList())));
                    break;
            }

            this.code.append(")");
        }
    }

    public void addWrite(String idf, String funcName, String type, String... indexes) {
        if(active) {
            this.code.append(generateIndent(this.indent));
            this.code.append("printf(");

            switch(type) {
                case "int":
                case "bool":
                    this.code.append("\"%d\\n\", ");
                    try {
                        Integer.parseInt(idf);
                        this.code.append(idf);
                    } catch (Exception e) {
                        if(idf.equals("true") || idf.equals("false")) {
                            this.code.append((idf.equals("true") ? 1 : 0));
                        } else {
                            if(this.refSymbols.contains(idf)){
                                this.code.append("*");
                            }
                            this.code.append(idf);
                        }
                    }
                    break;
                case "string":
                    this.code.append("\"%s\\n\", ");
                    this.code.append(idf);
                    break;
                case "ARRAY":
                    this.code.append("\"%d\\n\", &");
                    this.code.append(arrayCode(idf, funcName, Arrays.stream(indexes).collect(Collectors.toList())));
                    break;
            }

            this.code.append(")");
        }
    }

    public void enterBloc(){
        if(active) {
            this.code.append(generateIndent(this.indent));
            this.code.append("{");
            this.code.append(System.lineSeparator());

            this.indent++;
        }
    }

    public void exitBloc(){
        if(active) {
            this.indent--;

            this.code.append(generateIndent(this.indent));
            this.code.append("}");
            this.code.append(System.lineSeparator());
            this.code.append(System.lineSeparator());
        }
    }

    public void resetRefSymbols(){
        this.refSymbols.clear();
    }

    public void stopGenerator() {
        this.active = false;
    }

    public void writeCodeInFile(String path){
        if(active) {
            try {
                BufferedWriter writer = new BufferedWriter(new FileWriter(path));
                writer.write(this.code.toString());
                writer.close();
            } catch (java.io.IOException e) {
                System.err.println("Error while writing " + path + " : " + e.getMessage());
            }
        }
    }

    public String getCode() {
        if(active){
            return this.code.toString();
        } else {
            return "Please fix errors above to get the generated code.";
        }
    }
}
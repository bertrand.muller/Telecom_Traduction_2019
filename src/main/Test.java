package main;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.DOTTreeGenerator;
import org.antlr.stringtemplate.StringTemplate;
import org.antlr.tool.Message;
import tds.*;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.lang.reflect.Array;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Test {
    public static GeneratorC generator;
    public static ArrayList<String> errors;
    private static boolean isReturn;

    public static void main(String[] args) throws Exception {
        isReturn = false;
        Locale.setDefault(Locale.ENGLISH);

        // PARSE INPUT AND BUILD AST
        //ANTLRInputStream input = new ANTLRInputStream(System.in);
        // ANTLRFileStream input = new ANTLRFileStream("src/examples/SubjectExample.leac", "UTF8");
        ANTLRFileStream input = new ANTLRFileStream(args[0], "UTF8");
        ProjetLexer lexer = new ProjetLexer(input); // create lexer
        // create a buffer of tokens pulled from the lexer
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        ProjetParser parser = new ProjetParser(tokens); // create parser
        ProjetParser.program_return r = parser.program(); // parse rule prog
        // WALK TREE
        // get the tree from the return structure for rule prog
        CommonTree t = (CommonTree) r.getTree();

        DOTTreeGenerator dotGen = new DOTTreeGenerator();
        StringTemplate dotST = dotGen.toDOT(t);
        BufferedWriter writerDOT = new BufferedWriter(new FileWriter("ast.dot"));
        writerDOT.write(dotST.toString());
        writerDOT.close();

        generator = new GeneratorC();
        errors = new ArrayList<>();
        generator.initializeCode();

        TDS tds = new TDS();
        //try { // TODO remove comment
            recursive(t, tds);
        //}catch (Exception e){
        //    System.err.println("Error : syntax error");
        //}
    }

    public static String recursive(CommonTree t, TDS tds) {
        String tokenText;
        if(t.getParent() == null){ // first node (nil)
            tokenText = "PROG";
        }else{
            tokenText = t.getToken().getText();
        }
        if(isReturn && !isDefinedOrConstant(t, tds)){
            printError("NOT_REACHABLED",t.getLine());
        }
        switch (tokenText) {
            case "PROG":
                int nbChildrenProg = t.getChildCount();
                for(int i = 0; i < nbChildrenProg; i++) {
                    CommonTree progChild = (CommonTree) t.getChild(i);
                    recursive(progChild, tds);
                }
                break;
            case "VARDECLIST":
                int nbChildrenVarDecList = t.getChildCount();
                for(int i = 0; i < nbChildrenVarDecList; i++) {
                    CommonTree vardectree = (CommonTree) t.getChild(i);
                    vardec(vardectree, tds);
                }
                break;
            case "ARRAYVAL":
                List<String> arrayval = t.getChildren().stream().map(c -> getText((CommonTree)(c))).collect(Collectors.toList());
                arrayval.remove(0);
                if(isDefined(t, tds)) {
                    isInitialized(t, tds);
                    String type = getType(t, tds);
                    Symbole symbole = tds.getSymboleByIDF(getName(t));
                    if (symbole != null) {
                        if (symbole.getType().contains("array[")) {
                            generator.addExprArray(getName(t), getFunctionName(t, tds), arrayval);
                        }
                    }
                    return type;
                }else{
                    printError("UNDEFINED_VARIABLE",t.getLine(), getName(t));
                }
                return "error";
            case "FUNCS":
                int nbChildrenFuncs = t.getChildCount();
                for(int i = 0; i < nbChildrenFuncs; i++) {
                    CommonTree fundectree = (CommonTree) t.getChild(i);
                    fundec(fundectree, tds);
                }
                break;
            case "ELSE":
            case "BODY":
                int nbChildrenBody = t.getChildCount();
                if(t.getParent().getParent() == null) {
                    generator.addMainFunction();
                    generator.enterBloc();
                }
                for(int i = 0; i < nbChildrenBody; i++) {
                    CommonTree childBody = (CommonTree) t.getChild(i);
                    recursive(childBody, tds);
                }
                if(t.getParent().getParent() == null) {
                    generator.exitBloc();
                }
                break;
            case "AFFECT":
                affect(t, tds);
                break;
            case "WHILE":
                whileF(t, tds);
                break;
            case "IF":
                ifF(t, tds);
                break;
            case "WRITE":
                write(t, tds);
                break;
            case "READ":
                read(t, tds);
                break;
            case "RETURN":
                returnF(t, tds);
                break;
            case "CALLFUNC":
                return callfunc(t, tds);
            case "not":
                generator.addExpr("!(");
                if(!recursive((CommonTree) t.getChild(0), tds).equals("bool")) {
                    printError("OPERATOR_TYPE_EXPECTED", t.getLine(), "bool", getType(t, tds));
                }
                generator.addExpr(")");
                return "bool";
            case "-":
                if(t.getChildCount() == 1) {
                    generator.addExpr("-(");
                    if (!recursive((CommonTree) t.getChild(0), tds).equals("int")) {
                        printError("OPERATOR_TYPE_EXPECTED", t.getLine(), "int", getType((CommonTree) t.getChild(0), tds));
                    }
                    generator.addExpr(")");
                    return "int";
                }
            case "+":
            case "*":
            case "/":
            case "^":
                checkOperator(t, tds, "int", 2);
                return "int";
            case "==":
            case "!=":
            case "<=":
            case ">=":
            case "<":
            case ">":
            case "and":
            case "or":
                checkOperator(t, tds, "bool", 2);
                return "bool";
            case "EOF": // fin du fichier
                generator.writeCodeInFile("output.c");
                System.out.println(generator.getCode());
                break;
            default: // variable
                isInitialized(t, tds);
                generator.addExpr(getName(t));
                return getType(t, tds);
        }
        return null;
    }

    public static void whileF(CommonTree t, TDS tds) {
        generator.addWhileBegin();
        condition(t, tds);
        isReturn = false;
    }

    public static void ifF(CommonTree t, TDS tds) {
        generator.addIfBegin();
        condition(t, tds);
        boolean previousReturn = isReturn;
        isReturn = false;

        if(t.getChild(2) != null) {
            CommonTree elseB = (CommonTree) t.getChild(2);
            generator.addElseBegin();
            generator.enterBloc();
            recursive(elseB, tds);
            if(!previousReturn || !isReturn){
                isReturn = false;
            }
            generator.exitBloc();
        }
    }

    public static void condition(CommonTree t, TDS tds){
        CommonTree cond = (CommonTree) t.getChild(0);
        CommonTree body = (CommonTree) t.getChild(1);

        String condType = recursive(cond, tds);
        if(!condType.equals("bool")){
            printError("WRONG_CONDITION_TYPE", t.getLine(), condType);
        }
        generator.endWhifInstruction();

        generator.enterBloc();
        recursive(body, tds);
        generator.exitBloc();
    }

    public static void returnF(CommonTree t, TDS tds) {
        CommonTree child = (CommonTree) t.getChild(0);
        CommonTree function = getFunction(t, tds);
        String functionName = getText((CommonTree) function.getChild(1));
        String functionType = getText((CommonTree) function.getChild(0));
        generator.addReturn();

        String returnType = recursive(child, tds);

        if(returnType.equals("error")) {
            String variableName = getText((CommonTree) t.getChild(0));
            printError("UNDEFINED_VARIABLE", t.getLine(), variableName);
        } else if(!functionType.equals(returnType)) {
            printError("RETURN_TYPE_MISMATCH", t.getLine(), functionName, functionType, returnType);
        }
        generator.endGenericInstruction();
        isReturn = true;
    }

    public static void read(CommonTree t, TDS tds) {
        CommonTree child = (CommonTree) t.getChild(0);
        String childName = getText(child);
        String childType = getType(child, tds);

        boolean defined = isDefined(child, tds);
        if(!defined) {
            printError("UNDEFINED_VARIABLE", t.getLine(), getText(child));
        }

        if(childName.equals("ARRAYVAL")) {
            CommonTree childArray = (CommonTree) child.getChild(0);
            childName = getText(childArray);
            childType = "ARRAY";
            int nbChildren = child.getChildCount();
            String[] indexes = new String[nbChildren-1];
            for(int i = 1; i < nbChildren; i++) {
                indexes[i-1] = getText((CommonTree) child.getChild(i));
            }
            generator.addRead(childName, getFunctionName(child, tds), childType, indexes);
        } else {
            generator.addRead(childName, getFunctionName(child, tds), childType);
        }

        generator.endGenericInstruction();
    }

    public static void write(CommonTree t, TDS tds) {
        CommonTree child = (CommonTree) t.getChild(0);
        String childName = getText(child);
        String childType = getType(child, tds);

        boolean defined = isDefinedOrConstant(child, tds);
        if(!defined) {
            printError("UNDEFINED_VARIABLE", t.getLine(), getText(child));
        }else{
            isInitialized(child, tds);
            if(getType(child, tds).contains("array[")){
                printError("WRITE_ARRAY", t.getLine());
            }
        }

        if(childName.equals("ARRAYVAL")) {
            childName = getName(child);
            childType = "ARRAY";
            int nbChildren = child.getChildCount();
            String[] indexes = new String[nbChildren-1];
            for(int i = 1; i < nbChildren; i++) {
                indexes[i-1] = getText((CommonTree) child.getChild(i));
            }
            generator.addWrite(childName, getFunctionName(child, tds), childType, indexes);
        } else {
            generator.addWrite(childName, getFunctionName(child, tds),childType);
        }

        generator.endGenericInstruction();
    }

    public static String callfunc(CommonTree t, TDS tds){
        generator.addCallFunc(getName(t));
        areChildrenDefined(t, tds);
        generator.endCallFunc();
        return getType((CommonTree) t.getChild(0), tds);
    }

    public static void checkOperator(CommonTree t, TDS tds, String type, int nb){
        CommonTree leftExpr = (CommonTree) t.getChild(0);
        CommonTree rightExpr = (CommonTree) t.getChild(1);

        String operatorText = getText(t);
        if(operatorText.equals("^")){
            generator.addExpr("pow");
        }
        generator.addExpr("(");
        String leftType = recursive(leftExpr, tds);
        switch (operatorText){
            case "^":
                generator.addExpr(",");
                break;
            case "or":
                generator.addExpr(" || ");
                break;
            case "and":
                generator.addExpr(" && ");
                break;
            default:
                generator.addExpr(" "+operatorText+" ");
        }
        String rightType = recursive(rightExpr, tds);
        generator.addExpr(")");

        if(getText(t).equals("/")) {
            if(getText(rightExpr).equals("0")) {
                printError("DIVISION_BY_ZERO", t.getLine());
            }
        }

        if(type.equals("int")){
            if(!leftType.equals("int")){
                printError("OPERATOR_INT_EXPECTED", t.getLine(), "Left", leftType);
            }else if(!rightType.equals("int")){
                printError("OPERATOR_INT_EXPECTED", t.getLine(), "Right", rightType);
            }
        }

        if (!leftType.equals(rightType)) {
            printError("OPERATOR_TYPE_NOT_EQUALS", t.getLine(), leftType, rightType);
        }
    }

    public static void fundec(CommonTree t, TDS tds){
        String type = getText(((CommonTree)t.getChild(0)));
        String name = getText(((CommonTree)t.getChild(1)));
        if(TDSUtils.getSymbolInScope(name, tds) != null){
            printError("ALREADY_DEFINED", t.getLine(), name);
        }
        CommonTree arglist = (CommonTree)t.getChild(2);
        ArrayList<Argument> arguments = arglist(arglist, tds);

        generator.addFunction(name, type, arguments);
        TDS tdsFunction = tds.addFunction(name, arguments, type);

        CommonTree vardeclist = (CommonTree)t.getChild(3);
        int nbChildren = vardeclist.getChildCount();
        for(int i = 0; i < nbChildren; i++) {
            CommonTree vardectree = (CommonTree) vardeclist.getChild(i);
            vardec(vardectree, tdsFunction);
        }

        CommonTree body = (CommonTree) t.getChild(4);
        isReturn = false;
        recursive(body, tdsFunction);
        if(!isReturn && !type.equals("void")){
            printError("NO_RETURN", t.getLine(), name);
        }
        isReturn = false;
        generator.exitBloc();
        generator.resetRefSymbols();
    }

    public static void affect(CommonTree t, TDS tds) {
        CommonTree destTree = (CommonTree) t.getChild(0);
        CommonTree expr = (CommonTree) t.getChild(1);

        String destType = getType(destTree, tds);
        if(destType == null){
            printError("UNKNOWN_VARIABLE", t.getLine(), getName(destTree), t.getLine());
        }
        if(getText(destTree).equals("ARRAYVAL")){
            List<String> arrayval = destTree.getChildren().stream().map(c -> getText((CommonTree)(c))).collect(Collectors.toList());
            arrayval.remove(0);
            areChildrenDefined(destTree, tds);
            generator.addAffectLeftMemberArray(getName(destTree), getFunctionName(destTree, tds), arrayval);
        } else {
            generator.addAffectLeftMemberPrimitive(getText(destTree));
        }

        // check type with second member
        String exprType = recursive(expr, tds);
        if(!destType.equals(exprType)){
            printError("AFFECT_TYPE_MISMATCH", t.getLine(), getName(destTree), destType, getName(expr), exprType);
        }
        Symbole symbole = tds.getSymboleByIDF(getName(destTree));
        if(symbole != null) {
            symbole.initialize();
        }
        generator.endGenericInstruction();
    }

    public static void areChildrenDefined(CommonTree t, TDS tds){
        int nbChildren = t.getChildCount();
        CommonTree child;
        String childType;
        Symbole symbole = TDSUtils.getSymbolInScope(getName(t), tds);
        if(symbole != null) {
            if (isArrayval(t)) { // arrayval
                if (symbole.getType().contains("array[")) {
                    int shapeSize = ((SymboleArray) TDSUtils.getSymbolInScope(getName(t), tds)).getShape().size();
                    if (shapeSize < nbChildren - 1) {
                        printError("WRONG_DIMENSION_NUMBER", t.getLine(), shapeSize, nbChildren - 1);
                    }

                    for (int i = 1; i < nbChildren; i++) {
                        child = (CommonTree) t.getChild(i);
                        if (!isDefinedOrConstant(child, tds)) {
                            printError("UNDEFINED_VARIABLE", t.getLine(), getName(child));
                        }
                        isInitialized(child, tds);
                        childType = getType(child, tds);
                        if (!childType.equals("int")) {
                            printError("WRONG_DIMENSION_TYPE", t.getLine(), childType);
                        }
                    }

                    checkBounds(t, tds, nbChildren);
                }
            } else { // function
                ArrayList<Argument> arguments = ((SymboleFunction) symbole).getArguments();
                int argumentSize = arguments.size();
                if (argumentSize != nbChildren - 1) {
                    printError("WRONG_PARAMETER_NUMBER", t.getLine(), getName(t), argumentSize, nbChildren - 1);
                }

                String parameterType;
                Argument argument;
                for (int h = 1; h < nbChildren && h <= argumentSize; h++) {
                    child = (CommonTree) t.getChild(h);
                    childType = getType(child, tds);
                    argument = arguments.get(h - 1);
                    parameterType = argument.getType();
                    if (!childType.equals(parameterType)) {
                        printError("WRONG_PARAMETER_TYPE", t.getLine(), argument.getIdf(), getName(t), parameterType, childType);
                    }

                    if (isArrayval(child)) {
                        List<String> arrayval = new ArrayList<>();
                        CommonTree arrayChild;
                        for (int i = 1; i < child.getChildCount(); i++) {
                            arrayChild = (CommonTree) child.getChild(i);
                            isInitialized(arrayChild, tds);
                            arrayval.add(getName(arrayChild));
                        }
                        generator.addFuncArgumentArray(getName(child), getFunctionName(child, tds), argument.isRefmode(), childType, arrayval);
                    } else if (getText(child).equals("CALLFUNC")) {
                        recursive(child, tds);
                    } else {
                        isInitialized(child, tds);
                        generator.addFuncArgument(getName(child), argument.isRefmode(), childType);
                    }
                    if (h != nbChildren - 1) {
                        generator.addExpr(", ");
                    }
                }
            }
        }
    }

    public static void checkBounds(CommonTree t, TDS tds, int nbChildren){
        String idf = getName((CommonTree) t.getChild(0));
        SymboleArray sArray = (SymboleArray) TDSUtils.getSymbolInScope(idf, tds);

        for (int i = 1; i < nbChildren && i <= sArray.getShape().size(); i++) {
            CommonTree index = (CommonTree) t.getChild(i);

            if (isConstant(index)) {
                int inf = sArray.getShape().get(i - 1)[0];
                int sup = sArray.getShape().get(i - 1)[1];

                if (Integer.parseInt(getText(index)) < inf) {
                    printError("INDEX_OUT_OF_BOUND", t.getLine(), Integer.parseInt(getText(index)), "minimum", inf);
                }

                if (Integer.parseInt(getText(index)) > sup) {
                    printError("INDEX_OUT_OF_BOUND", t.getLine(), Integer.parseInt(getText(index)), "maximum", sup);
                }
            }
        }
    }

    public static ArrayList<Argument> arglist(CommonTree t, TDS tds) {
        int nbChildren = t.getChildCount();
        ArrayList<Argument> arguments = new ArrayList<>();
        for(int i = 0; i < nbChildren; i=i+3) {
            String name = getText(((CommonTree)t.getChild(i)));
            if(TDSUtils.getSymbolInScope(name, tds) != null){
            }else{
                for (Argument argument : arguments){
                    if(argument.getIdf().equals(name)){
                        printError("ALREADY_DEFINED", t.getLine(), name);
                        break;
                    }
                }
            }
            String type = getText(((CommonTree)t.getChild(i+1)));
            boolean refmode = getText(((CommonTree)t.getChild(i+2))).equals("REFTRUE");

            switch(type) {
                case "int":
                case "bool":
                    arguments.add(new ArgumentPrimitive(name, type, refmode));
                    break;
                case "void":
                    printError("TYPE_VOID_NOT_ALLOWED", t.getLine());
                    break;
                case "ARRAY":
                    arguments.add(arglist_array(t, i, refmode));
                    arguments.toString();
                    break;
            }
        }
        return arguments;
    }

    public static ArgumentArray arglist_array(CommonTree t, int index, boolean refmode){
        CommonTree array = ((CommonTree)t.getChild(index+1));
        String type = getText(((CommonTree)array.getChild(0)));
        ArrayList<int[]> shape = new ArrayList<>();

        if(type.equals("int") || type.equals("bool")){
            int nbChildren =  array.getChildCount();
            for(int i = 1; i < nbChildren; i=i+2) {
                String rangeLeft = getText(((CommonTree) array.getChild(i)));
                String rangeRight = getText(((CommonTree) array.getChild(i+1)));
                int[] indexes = new int[2];
                indexes[0] = Integer.parseInt(rangeLeft);
                indexes[1] = Integer.parseInt(rangeRight);
                if(indexes[0] >= indexes[1]) {
                    String name = getText((CommonTree) t.getChild(index));
                    printError("WRONG_INDEXES", t.getLine(), name);
                }
                shape.add(indexes);
            }
        }

        String name = getText((CommonTree)t.getChild(index));
        return new ArgumentArray(name, type, shape, refmode);
    }

    public static void vardec(CommonTree t, TDS tds){
        String type = getText(((CommonTree)t.getChild(0)));
        switch (type) {
            case "int":
            case "bool":
                vardec_intBool(t, tds, type);
                break;
            case "void":
                printError("TYPE_VOID_NOT_ALLOWED", t.getLine());
                break;
            case "ARRAY":
                vardec_array(t, tds);
                break;
        }
    }

    public static void vardec_intBool(CommonTree t, TDS tds, String type){
        int nbChildren =  t.getChildCount();
        for(int i = 1; i<nbChildren; i++){
            String name = getText((CommonTree)t.getChild(i));
            if(tds.getSymboleByIDF(name) != null){
                printError("ALREADY_DEFINED", t.getLine(), name);
            }
            generator.addPrimitiveVariable(name, type);
            switch (type){
                case "int":
                    tds.addInteger(name);
                    break;
                case "bool":
                    tds.addBoolean(name);
                    break;
            }
        }
    }

    public static void vardec_array(CommonTree t, TDS tds){
        int nbChildrenParent =  t.getChildCount();
        CommonTree array = (CommonTree)t.getChild(0);
        String type = getText(((CommonTree)array.getChild(0)));
        ArrayList<int[]> shape = new ArrayList<>();
        if(type.equals("int") || type.equals("bool")){
            int nbChildren =  array.getChildCount();
            for(int i = 1; i < nbChildren; i+=2){
                int tab[] = new int[2];
                tab[0] = Integer.parseInt(getText(((CommonTree)array.getChild(i))));
                tab[1] = Integer.parseInt(getText(((CommonTree)array.getChild(i+1))));
                if(tab[0] >= tab[1]) {
                    String name = getText((CommonTree) t.getChild(1));
                    printError("WRONG_INDEXES", t.getLine(), name);
                }
                shape.add(tab);
            }
        }else{
            printError("TYPE_VOID_NOT_ALLOWED", t.getLine());
        }

        for(int i = 1; i<nbChildrenParent; i++){
            CommonTree child = (CommonTree)t.getChild(i);
            String name = getText(child);
            if(TDSUtils.getSymbolInScope(name, tds) != null){
                printError("ALREADY_DEFINED", t.getLine(), name);
            }
            tds.addArray(name, type, shape);
            generator.addArrayVariable(name, type, shape, getFunctionName(child, tds));
        }
    }

    public static CommonTree getFunction(CommonTree t, TDS tds){
        while(t.getParent() != null){
            if(getText(t).equals("FUNCDEC")){
                return t;
            }else{
                t = (CommonTree) t.getParent();
            }
        }
        return null;
    }

    public static String getFunctionName(CommonTree t, TDS tds){
        String name = getName(t);
        while(t.getParent() != null){
            if(getText(t).equals("FUNCDEC")){
                String functionName = getName((CommonTree) t.getChild(1));
                if(tds.getSymboleByIDF(name) == null){
                    return "";
                }else{
                    return functionName;
                }
            }else{
                t = (CommonTree) t.getParent();
            }
        }
        return "";
    }

    public static String getText(CommonTree t){
        return t.getToken().getText();
    }

    public static String getName(CommonTree t){
        switch (getText(t)){
            case "ARRAYVAL":
            case "CALLFUNC":
                return getText((CommonTree)t.getChild(0));
            default:
                return getText(t);
        }
    }

    public static String getType(CommonTree t, TDS tds){
        String text = getText(t);
        if(text.equals("true") || text.equals("false")){
            return "bool";
        }else if(text.charAt(0) == '"' && text.charAt(text.length()-1) == '"'){
            return "string";
        }else{
            try{
                Integer.parseInt(text);
                return "int";
            }catch (Exception e){
                boolean arrayval = isArrayval(t);
                if(arrayval || getText(t).equals("CALLFUNC")) {
                    text = getText((CommonTree) t.getChild(0)); // name of array
                }
                Symbole symbole = TDSUtils.getSymbolInScope(text, tds);

                if(symbole == null){
                    switch(getText((CommonTree)t.getParent())){
                        case "CALLFUNC":
                            printError("UNDEFINED_FUNCTION", t.getLine(), text);
                            break;
                        case "AFFECT":
                            printError("UNDEFINED_VARIABLE", t.getLine(), text);
                            break;
                    }
                    return "error";
                }else {
                    String type = symbole.getType();
                    if(arrayval && t.getChildCount() > 1){
                        if(type.contains("array[")) {
                            String typeVal = type.split("\\[")[1].split("]")[0];
                            int typeRange = Integer.parseInt(type.split("\\[")[2].split("]")[0]);
                            if(typeRange - (t.getChildCount() - 1) == 0){ // type : int
                                return typeVal;
                            }else{ // type : array[int][1]
                                return "array["+typeVal+"]["+(typeRange- (t.getChildCount() - 1))+"]";
                            }
                        } else {
                            printError("NOT_ARRAY", t.getLine(), text);
                            return type;
                        }
                    }else {
                        return type;
                    }
                }
            }
        }
    }

    public static void isInitialized(CommonTree t, TDS tds){
        if(!isConstant(t)){
            Symbole symbole = TDSUtils.getSymbolInScope(getName(t), tds);
            if(symbole != null) {
                if (!symbole.isInitialized()){
                    if (tds.getParent() == null) {
                        printError("NOT_INITIALIZED_VARIABLE", t.getLine(), getName(t));
                    }
                }
            }
        }
    }

    public static boolean isArrayval(CommonTree t){
        return getText(t).equals("ARRAYVAL");
    }

    public static boolean isDefinedOrConstant(CommonTree t, TDS tds){
        if(isArrayval(t)){
            t = (CommonTree) t.getChild(0);
        }
        if(isConstant(t)){
            return true;
        }else{
            return isDefined(t, tds);
        }
    }

    public static boolean isDefined(CommonTree t, TDS tds){
        if(isArrayval(t)){
            areChildrenDefined(t, tds);
            t = (CommonTree) t.getChild(0);
        }
        return TDSUtils.isSymbolInScope(getText(t), tds);
    }

    public static boolean isConstant(CommonTree t){
        String text = getText(t);
        if(text.equals("true") || text.equals("false")){
            return true;
        }else if(text.charAt(0) == '"' && text.charAt(text.length()-1) == '"'){
            return true;
        }else{
            try{
                Integer.parseInt(text);
                return true;
            }catch (Exception e){
                return false;
            }
        }
    }

    public static void printError(String id, int line, Object... args) {
        ResourceBundle bundle = ResourceBundle.getBundle("errors/errors");
        String error = MessageFormat.format(bundle.getString(id), args)+ " - line "+line;
        System.err.println(error);
        errors.add(error);
        generator.stopGenerator();
    }
}

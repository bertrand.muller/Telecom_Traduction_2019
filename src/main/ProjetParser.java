// $ANTLR null /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g 2019-03-17 11:24:38
package main;
import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.tree.*;


@SuppressWarnings("all")
public class ProjetParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "AFFECT", "ARGLIST", "ARRAY", 
		"ARRAYVAL", "BODY", "CALLFUNC", "COMMENTAIRE", "COND", "CSTE", "ELSE", 
		"EXPR", "FUNCDEC", "FUNCS", "IDF", "IF", "READ", "REFFALSE", "REFTRUE", 
		"RETURN", "VARDEC", "VARDECLIST", "WHILE", "WHITESPACE", "WRITE", "'!='", 
		"'('", "')'", "'*'", "'+'", "','", "'-'", "'..'", "'/'", "':'", "';'", 
		"'<'", "'<='", "'='", "'=='", "'>'", "'>='", "'['", "']'", "'^'", "'and'", 
		"'array'", "'bool'", "'do'", "'else'", "'function'", "'if'", "'int'", 
		"'not'", "'of'", "'or'", "'program'", "'read'", "'ref'", "'return'", "'then'", 
		"'var'", "'void'", "'while'", "'write'", "'{'", "'}'"
	};
	public static final int EOF=-1;
	public static final int T__28=28;
	public static final int T__29=29;
	public static final int T__30=30;
	public static final int T__31=31;
	public static final int T__32=32;
	public static final int T__33=33;
	public static final int T__34=34;
	public static final int T__35=35;
	public static final int T__36=36;
	public static final int T__37=37;
	public static final int T__38=38;
	public static final int T__39=39;
	public static final int T__40=40;
	public static final int T__41=41;
	public static final int T__42=42;
	public static final int T__43=43;
	public static final int T__44=44;
	public static final int T__45=45;
	public static final int T__46=46;
	public static final int T__47=47;
	public static final int T__48=48;
	public static final int T__49=49;
	public static final int T__50=50;
	public static final int T__51=51;
	public static final int T__52=52;
	public static final int T__53=53;
	public static final int T__54=54;
	public static final int T__55=55;
	public static final int T__56=56;
	public static final int T__57=57;
	public static final int T__58=58;
	public static final int T__59=59;
	public static final int T__60=60;
	public static final int T__61=61;
	public static final int T__62=62;
	public static final int T__63=63;
	public static final int T__64=64;
	public static final int T__65=65;
	public static final int T__66=66;
	public static final int T__67=67;
	public static final int T__68=68;
	public static final int T__69=69;
	public static final int AFFECT=4;
	public static final int ARGLIST=5;
	public static final int ARRAY=6;
	public static final int ARRAYVAL=7;
	public static final int BODY=8;
	public static final int CALLFUNC=9;
	public static final int COMMENTAIRE=10;
	public static final int COND=11;
	public static final int CSTE=12;
	public static final int ELSE=13;
	public static final int EXPR=14;
	public static final int FUNCDEC=15;
	public static final int FUNCS=16;
	public static final int IDF=17;
	public static final int IF=18;
	public static final int READ=19;
	public static final int REFFALSE=20;
	public static final int REFTRUE=21;
	public static final int RETURN=22;
	public static final int VARDEC=23;
	public static final int VARDECLIST=24;
	public static final int WHILE=25;
	public static final int WHITESPACE=26;
	public static final int WRITE=27;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public ProjetParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public ProjetParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	protected TreeAdaptor adaptor = new CommonTreeAdaptor();

	public void setTreeAdaptor(TreeAdaptor adaptor) {
		this.adaptor = adaptor;
	}
	public TreeAdaptor getTreeAdaptor() {
		return adaptor;
	}
	@Override public String[] getTokenNames() { return ProjetParser.tokenNames; }
	@Override public String getGrammarFileName() { return "/home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g"; }


	public static class program_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "program"
	// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:32:1: program : 'program' IDF ( varsuitdecl )* ( funcdecl )* instr -> ^( VARDECLIST ( varsuitdecl )* ) ^( FUNCS ( funcdecl )* ) ^( BODY instr ) EOF ;
	public final ProjetParser.program_return program() throws RecognitionException {
		ProjetParser.program_return retval = new ProjetParser.program_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token string_literal1=null;
		Token IDF2=null;
		ParserRuleReturnScope varsuitdecl3 =null;
		ParserRuleReturnScope funcdecl4 =null;
		ParserRuleReturnScope instr5 =null;

		CommonTree string_literal1_tree=null;
		CommonTree IDF2_tree=null;
		RewriteRuleTokenStream stream_59=new RewriteRuleTokenStream(adaptor,"token 59");
		RewriteRuleTokenStream stream_IDF=new RewriteRuleTokenStream(adaptor,"token IDF");
		RewriteRuleSubtreeStream stream_funcdecl=new RewriteRuleSubtreeStream(adaptor,"rule funcdecl");
		RewriteRuleSubtreeStream stream_instr=new RewriteRuleSubtreeStream(adaptor,"rule instr");
		RewriteRuleSubtreeStream stream_varsuitdecl=new RewriteRuleSubtreeStream(adaptor,"rule varsuitdecl");

		try {
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:33:2: ( 'program' IDF ( varsuitdecl )* ( funcdecl )* instr -> ^( VARDECLIST ( varsuitdecl )* ) ^( FUNCS ( funcdecl )* ) ^( BODY instr ) EOF )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:33:4: 'program' IDF ( varsuitdecl )* ( funcdecl )* instr
			{
			string_literal1=(Token)match(input,59,FOLLOW_59_in_program121);  
			stream_59.add(string_literal1);

			IDF2=(Token)match(input,IDF,FOLLOW_IDF_in_program123);  
			stream_IDF.add(IDF2);

			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:33:18: ( varsuitdecl )*
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( (LA1_0==64) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:33:18: varsuitdecl
					{
					pushFollow(FOLLOW_varsuitdecl_in_program125);
					varsuitdecl3=varsuitdecl();
					state._fsp--;

					stream_varsuitdecl.add(varsuitdecl3.getTree());
					}
					break;

				default :
					break loop1;
				}
			}

			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:33:31: ( funcdecl )*
			loop2:
			while (true) {
				int alt2=2;
				int LA2_0 = input.LA(1);
				if ( (LA2_0==53) ) {
					alt2=1;
				}

				switch (alt2) {
				case 1 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:33:31: funcdecl
					{
					pushFollow(FOLLOW_funcdecl_in_program128);
					funcdecl4=funcdecl();
					state._fsp--;

					stream_funcdecl.add(funcdecl4.getTree());
					}
					break;

				default :
					break loop2;
				}
			}

			pushFollow(FOLLOW_instr_in_program131);
			instr5=instr();
			state._fsp--;

			stream_instr.add(instr5.getTree());
			// AST REWRITE
			// elements: funcdecl, varsuitdecl, instr
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 33:47: -> ^( VARDECLIST ( varsuitdecl )* ) ^( FUNCS ( funcdecl )* ) ^( BODY instr ) EOF
			{
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:33:50: ^( VARDECLIST ( varsuitdecl )* )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(VARDECLIST, "VARDECLIST"), root_1);
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:33:63: ( varsuitdecl )*
				while ( stream_varsuitdecl.hasNext() ) {
					adaptor.addChild(root_1, stream_varsuitdecl.nextTree());
				}
				stream_varsuitdecl.reset();

				adaptor.addChild(root_0, root_1);
				}

				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:33:77: ^( FUNCS ( funcdecl )* )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(FUNCS, "FUNCS"), root_1);
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:33:85: ( funcdecl )*
				while ( stream_funcdecl.hasNext() ) {
					adaptor.addChild(root_1, stream_funcdecl.nextTree());
				}
				stream_funcdecl.reset();

				adaptor.addChild(root_0, root_1);
				}

				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:33:96: ^( BODY instr )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(BODY, "BODY"), root_1);
				adaptor.addChild(root_1, stream_instr.nextTree());
				adaptor.addChild(root_0, root_1);
				}

				adaptor.addChild(root_0, (CommonTree)adaptor.create(EOF, "EOF"));
			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "program"


	public static class varsuitdecl_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "varsuitdecl"
	// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:36:1: varsuitdecl : 'var' identlist ':' typename ';' -> ^( VARDEC typename identlist ) ;
	public final ProjetParser.varsuitdecl_return varsuitdecl() throws RecognitionException {
		ProjetParser.varsuitdecl_return retval = new ProjetParser.varsuitdecl_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token string_literal6=null;
		Token char_literal8=null;
		Token char_literal10=null;
		ParserRuleReturnScope identlist7 =null;
		ParserRuleReturnScope typename9 =null;

		CommonTree string_literal6_tree=null;
		CommonTree char_literal8_tree=null;
		CommonTree char_literal10_tree=null;
		RewriteRuleTokenStream stream_37=new RewriteRuleTokenStream(adaptor,"token 37");
		RewriteRuleTokenStream stream_38=new RewriteRuleTokenStream(adaptor,"token 38");
		RewriteRuleTokenStream stream_64=new RewriteRuleTokenStream(adaptor,"token 64");
		RewriteRuleSubtreeStream stream_identlist=new RewriteRuleSubtreeStream(adaptor,"rule identlist");
		RewriteRuleSubtreeStream stream_typename=new RewriteRuleSubtreeStream(adaptor,"rule typename");

		try {
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:37:2: ( 'var' identlist ':' typename ';' -> ^( VARDEC typename identlist ) )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:37:4: 'var' identlist ':' typename ';'
			{
			string_literal6=(Token)match(input,64,FOLLOW_64_in_varsuitdecl169);  
			stream_64.add(string_literal6);

			pushFollow(FOLLOW_identlist_in_varsuitdecl171);
			identlist7=identlist();
			state._fsp--;

			stream_identlist.add(identlist7.getTree());
			char_literal8=(Token)match(input,37,FOLLOW_37_in_varsuitdecl173);  
			stream_37.add(char_literal8);

			pushFollow(FOLLOW_typename_in_varsuitdecl175);
			typename9=typename();
			state._fsp--;

			stream_typename.add(typename9.getTree());
			char_literal10=(Token)match(input,38,FOLLOW_38_in_varsuitdecl177);  
			stream_38.add(char_literal10);

			// AST REWRITE
			// elements: typename, identlist
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 37:37: -> ^( VARDEC typename identlist )
			{
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:37:40: ^( VARDEC typename identlist )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(VARDEC, "VARDEC"), root_1);
				adaptor.addChild(root_1, stream_typename.nextTree());
				adaptor.addChild(root_1, stream_identlist.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "varsuitdecl"


	public static class identlist_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "identlist"
	// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:40:1: identlist : IDF ( ',' identlist )? -> IDF ( identlist )? ;
	public final ProjetParser.identlist_return identlist() throws RecognitionException {
		ProjetParser.identlist_return retval = new ProjetParser.identlist_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token IDF11=null;
		Token char_literal12=null;
		ParserRuleReturnScope identlist13 =null;

		CommonTree IDF11_tree=null;
		CommonTree char_literal12_tree=null;
		RewriteRuleTokenStream stream_33=new RewriteRuleTokenStream(adaptor,"token 33");
		RewriteRuleTokenStream stream_IDF=new RewriteRuleTokenStream(adaptor,"token IDF");
		RewriteRuleSubtreeStream stream_identlist=new RewriteRuleSubtreeStream(adaptor,"rule identlist");

		try {
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:41:2: ( IDF ( ',' identlist )? -> IDF ( identlist )? )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:41:4: IDF ( ',' identlist )?
			{
			IDF11=(Token)match(input,IDF,FOLLOW_IDF_in_identlist202);  
			stream_IDF.add(IDF11);

			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:41:8: ( ',' identlist )?
			int alt3=2;
			int LA3_0 = input.LA(1);
			if ( (LA3_0==33) ) {
				alt3=1;
			}
			switch (alt3) {
				case 1 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:41:9: ',' identlist
					{
					char_literal12=(Token)match(input,33,FOLLOW_33_in_identlist205);  
					stream_33.add(char_literal12);

					pushFollow(FOLLOW_identlist_in_identlist207);
					identlist13=identlist();
					state._fsp--;

					stream_identlist.add(identlist13.getTree());
					}
					break;

			}

			// AST REWRITE
			// elements: identlist, IDF
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 41:25: -> IDF ( identlist )?
			{
				adaptor.addChild(root_0, stream_IDF.nextNode());
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:41:32: ( identlist )?
				if ( stream_identlist.hasNext() ) {
					adaptor.addChild(root_0, stream_identlist.nextTree());
				}
				stream_identlist.reset();

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "identlist"


	public static class typename_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "typename"
	// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:44:1: typename : ( atomtype -> atomtype | arraytype -> ^( ARRAY arraytype ) );
	public final ProjetParser.typename_return typename() throws RecognitionException {
		ProjetParser.typename_return retval = new ProjetParser.typename_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		ParserRuleReturnScope atomtype14 =null;
		ParserRuleReturnScope arraytype15 =null;

		RewriteRuleSubtreeStream stream_arraytype=new RewriteRuleSubtreeStream(adaptor,"rule arraytype");
		RewriteRuleSubtreeStream stream_atomtype=new RewriteRuleSubtreeStream(adaptor,"rule atomtype");

		try {
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:45:2: ( atomtype -> atomtype | arraytype -> ^( ARRAY arraytype ) )
			int alt4=2;
			int LA4_0 = input.LA(1);
			if ( (LA4_0==50||LA4_0==55||LA4_0==65) ) {
				alt4=1;
			}
			else if ( (LA4_0==49) ) {
				alt4=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 4, 0, input);
				throw nvae;
			}

			switch (alt4) {
				case 1 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:45:4: atomtype
					{
					pushFollow(FOLLOW_atomtype_in_typename228);
					atomtype14=atomtype();
					state._fsp--;

					stream_atomtype.add(atomtype14.getTree());
					// AST REWRITE
					// elements: atomtype
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 45:13: -> atomtype
					{
						adaptor.addChild(root_0, stream_atomtype.nextTree());
					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:46:4: arraytype
					{
					pushFollow(FOLLOW_arraytype_in_typename237);
					arraytype15=arraytype();
					state._fsp--;

					stream_arraytype.add(arraytype15.getTree());
					// AST REWRITE
					// elements: arraytype
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 46:14: -> ^( ARRAY arraytype )
					{
						// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:46:17: ^( ARRAY arraytype )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(ARRAY, "ARRAY"), root_1);
						adaptor.addChild(root_1, stream_arraytype.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "typename"


	public static class atomtype_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "atomtype"
	// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:49:1: atomtype : ( 'void' | 'bool' | 'int' );
	public final ProjetParser.atomtype_return atomtype() throws RecognitionException {
		ProjetParser.atomtype_return retval = new ProjetParser.atomtype_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token set16=null;

		CommonTree set16_tree=null;

		try {
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:50:2: ( 'void' | 'bool' | 'int' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:
			{
			root_0 = (CommonTree)adaptor.nil();


			set16=input.LT(1);
			if ( input.LA(1)==50||input.LA(1)==55||input.LA(1)==65 ) {
				input.consume();
				adaptor.addChild(root_0, (CommonTree)adaptor.create(set16));
				state.errorRecovery=false;
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "atomtype"


	public static class arraytype_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "arraytype"
	// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:55:1: arraytype : 'array' '[' rangelist ']' 'of' atomtype -> atomtype rangelist ;
	public final ProjetParser.arraytype_return arraytype() throws RecognitionException {
		ProjetParser.arraytype_return retval = new ProjetParser.arraytype_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token string_literal17=null;
		Token char_literal18=null;
		Token char_literal20=null;
		Token string_literal21=null;
		ParserRuleReturnScope rangelist19 =null;
		ParserRuleReturnScope atomtype22 =null;

		CommonTree string_literal17_tree=null;
		CommonTree char_literal18_tree=null;
		CommonTree char_literal20_tree=null;
		CommonTree string_literal21_tree=null;
		RewriteRuleTokenStream stream_45=new RewriteRuleTokenStream(adaptor,"token 45");
		RewriteRuleTokenStream stream_46=new RewriteRuleTokenStream(adaptor,"token 46");
		RewriteRuleTokenStream stream_57=new RewriteRuleTokenStream(adaptor,"token 57");
		RewriteRuleTokenStream stream_49=new RewriteRuleTokenStream(adaptor,"token 49");
		RewriteRuleSubtreeStream stream_rangelist=new RewriteRuleSubtreeStream(adaptor,"rule rangelist");
		RewriteRuleSubtreeStream stream_atomtype=new RewriteRuleSubtreeStream(adaptor,"rule atomtype");

		try {
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:56:2: ( 'array' '[' rangelist ']' 'of' atomtype -> atomtype rangelist )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:56:4: 'array' '[' rangelist ']' 'of' atomtype
			{
			string_literal17=(Token)match(input,49,FOLLOW_49_in_arraytype279);  
			stream_49.add(string_literal17);

			char_literal18=(Token)match(input,45,FOLLOW_45_in_arraytype281);  
			stream_45.add(char_literal18);

			pushFollow(FOLLOW_rangelist_in_arraytype283);
			rangelist19=rangelist();
			state._fsp--;

			stream_rangelist.add(rangelist19.getTree());
			char_literal20=(Token)match(input,46,FOLLOW_46_in_arraytype285);  
			stream_46.add(char_literal20);

			string_literal21=(Token)match(input,57,FOLLOW_57_in_arraytype287);  
			stream_57.add(string_literal21);

			pushFollow(FOLLOW_atomtype_in_arraytype289);
			atomtype22=atomtype();
			state._fsp--;

			stream_atomtype.add(atomtype22.getTree());
			// AST REWRITE
			// elements: atomtype, rangelist
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 56:44: -> atomtype rangelist
			{
				adaptor.addChild(root_0, stream_atomtype.nextTree());
				adaptor.addChild(root_0, stream_rangelist.nextTree());
			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "arraytype"


	public static class rangelist_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "rangelist"
	// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:59:1: rangelist : start= CSTE '..' stop= CSTE ( ',' rangelist )? -> $start $stop ( rangelist )? ;
	public final ProjetParser.rangelist_return rangelist() throws RecognitionException {
		ProjetParser.rangelist_return retval = new ProjetParser.rangelist_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token start=null;
		Token stop=null;
		Token string_literal23=null;
		Token char_literal24=null;
		ParserRuleReturnScope rangelist25 =null;

		CommonTree start_tree=null;
		CommonTree stop_tree=null;
		CommonTree string_literal23_tree=null;
		CommonTree char_literal24_tree=null;
		RewriteRuleTokenStream stream_33=new RewriteRuleTokenStream(adaptor,"token 33");
		RewriteRuleTokenStream stream_35=new RewriteRuleTokenStream(adaptor,"token 35");
		RewriteRuleTokenStream stream_CSTE=new RewriteRuleTokenStream(adaptor,"token CSTE");
		RewriteRuleSubtreeStream stream_rangelist=new RewriteRuleSubtreeStream(adaptor,"rule rangelist");

		try {
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:60:2: (start= CSTE '..' stop= CSTE ( ',' rangelist )? -> $start $stop ( rangelist )? )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:60:4: start= CSTE '..' stop= CSTE ( ',' rangelist )?
			{
			start=(Token)match(input,CSTE,FOLLOW_CSTE_in_rangelist309);  
			stream_CSTE.add(start);

			string_literal23=(Token)match(input,35,FOLLOW_35_in_rangelist311);  
			stream_35.add(string_literal23);

			stop=(Token)match(input,CSTE,FOLLOW_CSTE_in_rangelist315);  
			stream_CSTE.add(stop);

			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:60:30: ( ',' rangelist )?
			int alt5=2;
			int LA5_0 = input.LA(1);
			if ( (LA5_0==33) ) {
				alt5=1;
			}
			switch (alt5) {
				case 1 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:60:31: ',' rangelist
					{
					char_literal24=(Token)match(input,33,FOLLOW_33_in_rangelist318);  
					stream_33.add(char_literal24);

					pushFollow(FOLLOW_rangelist_in_rangelist320);
					rangelist25=rangelist();
					state._fsp--;

					stream_rangelist.add(rangelist25.getTree());
					}
					break;

			}

			// AST REWRITE
			// elements: start, stop, rangelist
			// token labels: stop, start
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleTokenStream stream_stop=new RewriteRuleTokenStream(adaptor,"token stop",stop);
			RewriteRuleTokenStream stream_start=new RewriteRuleTokenStream(adaptor,"token start",start);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 60:47: -> $start $stop ( rangelist )?
			{
				adaptor.addChild(root_0, stream_start.nextNode());
				adaptor.addChild(root_0, stream_stop.nextNode());
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:60:63: ( rangelist )?
				if ( stream_rangelist.hasNext() ) {
					adaptor.addChild(root_0, stream_rangelist.nextTree());
				}
				stream_rangelist.reset();

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "rangelist"


	public static class funcdecl_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "funcdecl"
	// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:63:1: funcdecl : 'function' IDF '(' arglist ')' ':' atomtype ( varsuitdecl )* '{' sequence '}' -> ^( FUNCDEC atomtype IDF ^( ARGLIST ( arglist )? ) ^( VARDECLIST ( varsuitdecl )* ) ^( BODY sequence ) ) ;
	public final ProjetParser.funcdecl_return funcdecl() throws RecognitionException {
		ProjetParser.funcdecl_return retval = new ProjetParser.funcdecl_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token string_literal26=null;
		Token IDF27=null;
		Token char_literal28=null;
		Token char_literal30=null;
		Token char_literal31=null;
		Token char_literal34=null;
		Token char_literal36=null;
		ParserRuleReturnScope arglist29 =null;
		ParserRuleReturnScope atomtype32 =null;
		ParserRuleReturnScope varsuitdecl33 =null;
		ParserRuleReturnScope sequence35 =null;

		CommonTree string_literal26_tree=null;
		CommonTree IDF27_tree=null;
		CommonTree char_literal28_tree=null;
		CommonTree char_literal30_tree=null;
		CommonTree char_literal31_tree=null;
		CommonTree char_literal34_tree=null;
		CommonTree char_literal36_tree=null;
		RewriteRuleTokenStream stream_68=new RewriteRuleTokenStream(adaptor,"token 68");
		RewriteRuleTokenStream stream_69=new RewriteRuleTokenStream(adaptor,"token 69");
		RewriteRuleTokenStream stream_37=new RewriteRuleTokenStream(adaptor,"token 37");
		RewriteRuleTokenStream stream_29=new RewriteRuleTokenStream(adaptor,"token 29");
		RewriteRuleTokenStream stream_IDF=new RewriteRuleTokenStream(adaptor,"token IDF");
		RewriteRuleTokenStream stream_30=new RewriteRuleTokenStream(adaptor,"token 30");
		RewriteRuleTokenStream stream_53=new RewriteRuleTokenStream(adaptor,"token 53");
		RewriteRuleSubtreeStream stream_sequence=new RewriteRuleSubtreeStream(adaptor,"rule sequence");
		RewriteRuleSubtreeStream stream_arglist=new RewriteRuleSubtreeStream(adaptor,"rule arglist");
		RewriteRuleSubtreeStream stream_varsuitdecl=new RewriteRuleSubtreeStream(adaptor,"rule varsuitdecl");
		RewriteRuleSubtreeStream stream_atomtype=new RewriteRuleSubtreeStream(adaptor,"rule atomtype");

		try {
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:64:2: ( 'function' IDF '(' arglist ')' ':' atomtype ( varsuitdecl )* '{' sequence '}' -> ^( FUNCDEC atomtype IDF ^( ARGLIST ( arglist )? ) ^( VARDECLIST ( varsuitdecl )* ) ^( BODY sequence ) ) )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:64:4: 'function' IDF '(' arglist ')' ':' atomtype ( varsuitdecl )* '{' sequence '}'
			{
			string_literal26=(Token)match(input,53,FOLLOW_53_in_funcdecl346);  
			stream_53.add(string_literal26);

			IDF27=(Token)match(input,IDF,FOLLOW_IDF_in_funcdecl348);  
			stream_IDF.add(IDF27);

			char_literal28=(Token)match(input,29,FOLLOW_29_in_funcdecl350);  
			stream_29.add(char_literal28);

			pushFollow(FOLLOW_arglist_in_funcdecl352);
			arglist29=arglist();
			state._fsp--;

			stream_arglist.add(arglist29.getTree());
			char_literal30=(Token)match(input,30,FOLLOW_30_in_funcdecl354);  
			stream_30.add(char_literal30);

			char_literal31=(Token)match(input,37,FOLLOW_37_in_funcdecl356);  
			stream_37.add(char_literal31);

			pushFollow(FOLLOW_atomtype_in_funcdecl358);
			atomtype32=atomtype();
			state._fsp--;

			stream_atomtype.add(atomtype32.getTree());
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:64:48: ( varsuitdecl )*
			loop6:
			while (true) {
				int alt6=2;
				int LA6_0 = input.LA(1);
				if ( (LA6_0==64) ) {
					alt6=1;
				}

				switch (alt6) {
				case 1 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:64:48: varsuitdecl
					{
					pushFollow(FOLLOW_varsuitdecl_in_funcdecl360);
					varsuitdecl33=varsuitdecl();
					state._fsp--;

					stream_varsuitdecl.add(varsuitdecl33.getTree());
					}
					break;

				default :
					break loop6;
				}
			}

			char_literal34=(Token)match(input,68,FOLLOW_68_in_funcdecl363);  
			stream_68.add(char_literal34);

			pushFollow(FOLLOW_sequence_in_funcdecl365);
			sequence35=sequence();
			state._fsp--;

			stream_sequence.add(sequence35.getTree());
			char_literal36=(Token)match(input,69,FOLLOW_69_in_funcdecl367);  
			stream_69.add(char_literal36);

			// AST REWRITE
			// elements: arglist, varsuitdecl, atomtype, IDF, sequence
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 64:78: -> ^( FUNCDEC atomtype IDF ^( ARGLIST ( arglist )? ) ^( VARDECLIST ( varsuitdecl )* ) ^( BODY sequence ) )
			{
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:64:81: ^( FUNCDEC atomtype IDF ^( ARGLIST ( arglist )? ) ^( VARDECLIST ( varsuitdecl )* ) ^( BODY sequence ) )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(FUNCDEC, "FUNCDEC"), root_1);
				adaptor.addChild(root_1, stream_atomtype.nextTree());
				adaptor.addChild(root_1, stream_IDF.nextNode());
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:64:104: ^( ARGLIST ( arglist )? )
				{
				CommonTree root_2 = (CommonTree)adaptor.nil();
				root_2 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(ARGLIST, "ARGLIST"), root_2);
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:64:114: ( arglist )?
				if ( stream_arglist.hasNext() ) {
					adaptor.addChild(root_2, stream_arglist.nextTree());
				}
				stream_arglist.reset();

				adaptor.addChild(root_1, root_2);
				}

				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:64:124: ^( VARDECLIST ( varsuitdecl )* )
				{
				CommonTree root_2 = (CommonTree)adaptor.nil();
				root_2 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(VARDECLIST, "VARDECLIST"), root_2);
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:64:137: ( varsuitdecl )*
				while ( stream_varsuitdecl.hasNext() ) {
					adaptor.addChild(root_2, stream_varsuitdecl.nextTree());
				}
				stream_varsuitdecl.reset();

				adaptor.addChild(root_1, root_2);
				}

				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:64:151: ^( BODY sequence )
				{
				CommonTree root_2 = (CommonTree)adaptor.nil();
				root_2 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(BODY, "BODY"), root_2);
				adaptor.addChild(root_2, stream_sequence.nextTree());
				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "funcdecl"


	public static class arglist_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "arglist"
	// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:67:1: arglist : ( arg ( ',' arglist )? -> arg ( arglist )? |);
	public final ProjetParser.arglist_return arglist() throws RecognitionException {
		ProjetParser.arglist_return retval = new ProjetParser.arglist_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token char_literal38=null;
		ParserRuleReturnScope arg37 =null;
		ParserRuleReturnScope arglist39 =null;

		CommonTree char_literal38_tree=null;
		RewriteRuleTokenStream stream_33=new RewriteRuleTokenStream(adaptor,"token 33");
		RewriteRuleSubtreeStream stream_arg=new RewriteRuleSubtreeStream(adaptor,"rule arg");
		RewriteRuleSubtreeStream stream_arglist=new RewriteRuleSubtreeStream(adaptor,"rule arglist");

		try {
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:68:2: ( arg ( ',' arglist )? -> arg ( arglist )? |)
			int alt8=2;
			int LA8_0 = input.LA(1);
			if ( (LA8_0==IDF||LA8_0==61) ) {
				alt8=1;
			}
			else if ( (LA8_0==30) ) {
				alt8=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 8, 0, input);
				throw nvae;
			}

			switch (alt8) {
				case 1 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:68:4: arg ( ',' arglist )?
					{
					pushFollow(FOLLOW_arg_in_arglist409);
					arg37=arg();
					state._fsp--;

					stream_arg.add(arg37.getTree());
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:68:8: ( ',' arglist )?
					int alt7=2;
					int LA7_0 = input.LA(1);
					if ( (LA7_0==33) ) {
						alt7=1;
					}
					switch (alt7) {
						case 1 :
							// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:68:9: ',' arglist
							{
							char_literal38=(Token)match(input,33,FOLLOW_33_in_arglist412);  
							stream_33.add(char_literal38);

							pushFollow(FOLLOW_arglist_in_arglist414);
							arglist39=arglist();
							state._fsp--;

							stream_arglist.add(arglist39.getTree());
							}
							break;

					}

					// AST REWRITE
					// elements: arg, arglist
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 68:23: -> arg ( arglist )?
					{
						adaptor.addChild(root_0, stream_arg.nextTree());
						// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:68:30: ( arglist )?
						if ( stream_arglist.hasNext() ) {
							adaptor.addChild(root_0, stream_arglist.nextTree());
						}
						stream_arglist.reset();

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:70:2: 
					{
					root_0 = (CommonTree)adaptor.nil();


					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "arglist"


	public static class arg_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "arg"
	// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:72:1: arg : ( IDF ':' typename -> IDF typename REFFALSE | 'ref' IDF ':' typename -> IDF typename REFTRUE );
	public final ProjetParser.arg_return arg() throws RecognitionException {
		ProjetParser.arg_return retval = new ProjetParser.arg_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token IDF40=null;
		Token char_literal41=null;
		Token string_literal43=null;
		Token IDF44=null;
		Token char_literal45=null;
		ParserRuleReturnScope typename42 =null;
		ParserRuleReturnScope typename46 =null;

		CommonTree IDF40_tree=null;
		CommonTree char_literal41_tree=null;
		CommonTree string_literal43_tree=null;
		CommonTree IDF44_tree=null;
		CommonTree char_literal45_tree=null;
		RewriteRuleTokenStream stream_37=new RewriteRuleTokenStream(adaptor,"token 37");
		RewriteRuleTokenStream stream_IDF=new RewriteRuleTokenStream(adaptor,"token IDF");
		RewriteRuleTokenStream stream_61=new RewriteRuleTokenStream(adaptor,"token 61");
		RewriteRuleSubtreeStream stream_typename=new RewriteRuleSubtreeStream(adaptor,"rule typename");

		try {
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:73:2: ( IDF ':' typename -> IDF typename REFFALSE | 'ref' IDF ':' typename -> IDF typename REFTRUE )
			int alt9=2;
			int LA9_0 = input.LA(1);
			if ( (LA9_0==IDF) ) {
				alt9=1;
			}
			else if ( (LA9_0==61) ) {
				alt9=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 9, 0, input);
				throw nvae;
			}

			switch (alt9) {
				case 1 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:73:4: IDF ':' typename
					{
					IDF40=(Token)match(input,IDF,FOLLOW_IDF_in_arg438);  
					stream_IDF.add(IDF40);

					char_literal41=(Token)match(input,37,FOLLOW_37_in_arg440);  
					stream_37.add(char_literal41);

					pushFollow(FOLLOW_typename_in_arg442);
					typename42=typename();
					state._fsp--;

					stream_typename.add(typename42.getTree());
					// AST REWRITE
					// elements: typename, IDF
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 73:21: -> IDF typename REFFALSE
					{
						adaptor.addChild(root_0, stream_IDF.nextNode());
						adaptor.addChild(root_0, stream_typename.nextTree());
						adaptor.addChild(root_0, (CommonTree)adaptor.create(REFFALSE, "REFFALSE"));
					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:74:4: 'ref' IDF ':' typename
					{
					string_literal43=(Token)match(input,61,FOLLOW_61_in_arg455);  
					stream_61.add(string_literal43);

					IDF44=(Token)match(input,IDF,FOLLOW_IDF_in_arg457);  
					stream_IDF.add(IDF44);

					char_literal45=(Token)match(input,37,FOLLOW_37_in_arg459);  
					stream_37.add(char_literal45);

					pushFollow(FOLLOW_typename_in_arg461);
					typename46=typename();
					state._fsp--;

					stream_typename.add(typename46.getTree());
					// AST REWRITE
					// elements: IDF, typename
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 74:27: -> IDF typename REFTRUE
					{
						adaptor.addChild(root_0, stream_IDF.nextNode());
						adaptor.addChild(root_0, stream_typename.nextTree());
						adaptor.addChild(root_0, (CommonTree)adaptor.create(REFTRUE, "REFTRUE"));
					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "arg"


	public static class instr_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "instr"
	// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:77:1: instr : ( 'if' expr1 'then' then= instr ( options {greedy=true; } : 'else' els= instr )? -> ^( IF expr1 ^( BODY $then) ( ^( ELSE $els) )? ) | 'while' expr1 'do' instr -> ^( WHILE expr1 ^( BODY instr ) ) | 'return' ( expr1 )? -> ^( RETURN expr1 ) | IDF ( '(' ( exprlist )? ')' -> ^( CALLFUNC IDF ( exprlist )? ) | ( '[' exprlist ']' '=' expr1 -> ^( AFFECT ^( ARRAYVAL IDF exprlist ) expr1 ) | '=' expr1 -> ^( AFFECT IDF expr1 ) ) ) | '{' ( sequence )? '}' -> ( sequence )? | 'read' lvalue -> ^( READ lvalue ) | 'write' ( lvalue | CSTE ) -> ^( WRITE ( lvalue )? ( CSTE )? ) );
	public final ProjetParser.instr_return instr() throws RecognitionException {
		ProjetParser.instr_return retval = new ProjetParser.instr_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token string_literal47=null;
		Token string_literal49=null;
		Token string_literal50=null;
		Token string_literal51=null;
		Token string_literal53=null;
		Token string_literal55=null;
		Token IDF57=null;
		Token char_literal58=null;
		Token char_literal60=null;
		Token char_literal61=null;
		Token char_literal63=null;
		Token char_literal64=null;
		Token char_literal66=null;
		Token char_literal68=null;
		Token char_literal70=null;
		Token string_literal71=null;
		Token string_literal73=null;
		Token CSTE75=null;
		ParserRuleReturnScope then =null;
		ParserRuleReturnScope els =null;
		ParserRuleReturnScope expr148 =null;
		ParserRuleReturnScope expr152 =null;
		ParserRuleReturnScope instr54 =null;
		ParserRuleReturnScope expr156 =null;
		ParserRuleReturnScope exprlist59 =null;
		ParserRuleReturnScope exprlist62 =null;
		ParserRuleReturnScope expr165 =null;
		ParserRuleReturnScope expr167 =null;
		ParserRuleReturnScope sequence69 =null;
		ParserRuleReturnScope lvalue72 =null;
		ParserRuleReturnScope lvalue74 =null;

		CommonTree string_literal47_tree=null;
		CommonTree string_literal49_tree=null;
		CommonTree string_literal50_tree=null;
		CommonTree string_literal51_tree=null;
		CommonTree string_literal53_tree=null;
		CommonTree string_literal55_tree=null;
		CommonTree IDF57_tree=null;
		CommonTree char_literal58_tree=null;
		CommonTree char_literal60_tree=null;
		CommonTree char_literal61_tree=null;
		CommonTree char_literal63_tree=null;
		CommonTree char_literal64_tree=null;
		CommonTree char_literal66_tree=null;
		CommonTree char_literal68_tree=null;
		CommonTree char_literal70_tree=null;
		CommonTree string_literal71_tree=null;
		CommonTree string_literal73_tree=null;
		CommonTree CSTE75_tree=null;
		RewriteRuleTokenStream stream_66=new RewriteRuleTokenStream(adaptor,"token 66");
		RewriteRuleTokenStream stream_45=new RewriteRuleTokenStream(adaptor,"token 45");
		RewriteRuleTokenStream stream_67=new RewriteRuleTokenStream(adaptor,"token 67");
		RewriteRuleTokenStream stream_46=new RewriteRuleTokenStream(adaptor,"token 46");
		RewriteRuleTokenStream stream_68=new RewriteRuleTokenStream(adaptor,"token 68");
		RewriteRuleTokenStream stream_69=new RewriteRuleTokenStream(adaptor,"token 69");
		RewriteRuleTokenStream stream_29=new RewriteRuleTokenStream(adaptor,"token 29");
		RewriteRuleTokenStream stream_IDF=new RewriteRuleTokenStream(adaptor,"token IDF");
		RewriteRuleTokenStream stream_60=new RewriteRuleTokenStream(adaptor,"token 60");
		RewriteRuleTokenStream stream_51=new RewriteRuleTokenStream(adaptor,"token 51");
		RewriteRuleTokenStream stream_62=new RewriteRuleTokenStream(adaptor,"token 62");
		RewriteRuleTokenStream stream_63=new RewriteRuleTokenStream(adaptor,"token 63");
		RewriteRuleTokenStream stream_52=new RewriteRuleTokenStream(adaptor,"token 52");
		RewriteRuleTokenStream stream_41=new RewriteRuleTokenStream(adaptor,"token 41");
		RewriteRuleTokenStream stream_30=new RewriteRuleTokenStream(adaptor,"token 30");
		RewriteRuleTokenStream stream_CSTE=new RewriteRuleTokenStream(adaptor,"token CSTE");
		RewriteRuleTokenStream stream_54=new RewriteRuleTokenStream(adaptor,"token 54");
		RewriteRuleSubtreeStream stream_sequence=new RewriteRuleSubtreeStream(adaptor,"rule sequence");
		RewriteRuleSubtreeStream stream_lvalue=new RewriteRuleSubtreeStream(adaptor,"rule lvalue");
		RewriteRuleSubtreeStream stream_instr=new RewriteRuleSubtreeStream(adaptor,"rule instr");
		RewriteRuleSubtreeStream stream_exprlist=new RewriteRuleSubtreeStream(adaptor,"rule exprlist");
		RewriteRuleSubtreeStream stream_expr1=new RewriteRuleSubtreeStream(adaptor,"rule expr1");

		try {
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:78:2: ( 'if' expr1 'then' then= instr ( options {greedy=true; } : 'else' els= instr )? -> ^( IF expr1 ^( BODY $then) ( ^( ELSE $els) )? ) | 'while' expr1 'do' instr -> ^( WHILE expr1 ^( BODY instr ) ) | 'return' ( expr1 )? -> ^( RETURN expr1 ) | IDF ( '(' ( exprlist )? ')' -> ^( CALLFUNC IDF ( exprlist )? ) | ( '[' exprlist ']' '=' expr1 -> ^( AFFECT ^( ARRAYVAL IDF exprlist ) expr1 ) | '=' expr1 -> ^( AFFECT IDF expr1 ) ) ) | '{' ( sequence )? '}' -> ( sequence )? | 'read' lvalue -> ^( READ lvalue ) | 'write' ( lvalue | CSTE ) -> ^( WRITE ( lvalue )? ( CSTE )? ) )
			int alt17=7;
			switch ( input.LA(1) ) {
			case 54:
				{
				alt17=1;
				}
				break;
			case 66:
				{
				alt17=2;
				}
				break;
			case 62:
				{
				alt17=3;
				}
				break;
			case IDF:
				{
				alt17=4;
				}
				break;
			case 68:
				{
				alt17=5;
				}
				break;
			case 60:
				{
				alt17=6;
				}
				break;
			case 67:
				{
				alt17=7;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 17, 0, input);
				throw nvae;
			}
			switch (alt17) {
				case 1 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:78:4: 'if' expr1 'then' then= instr ( options {greedy=true; } : 'else' els= instr )?
					{
					string_literal47=(Token)match(input,54,FOLLOW_54_in_instr481);  
					stream_54.add(string_literal47);

					pushFollow(FOLLOW_expr1_in_instr483);
					expr148=expr1();
					state._fsp--;

					stream_expr1.add(expr148.getTree());
					string_literal49=(Token)match(input,63,FOLLOW_63_in_instr485);  
					stream_63.add(string_literal49);

					pushFollow(FOLLOW_instr_in_instr489);
					then=instr();
					state._fsp--;

					stream_instr.add(then.getTree());
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:78:33: ( options {greedy=true; } : 'else' els= instr )?
					int alt10=2;
					int LA10_0 = input.LA(1);
					if ( (LA10_0==52) ) {
						alt10=1;
					}
					switch (alt10) {
						case 1 :
							// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:78:56: 'else' els= instr
							{
							string_literal50=(Token)match(input,52,FOLLOW_52_in_instr499);  
							stream_52.add(string_literal50);

							pushFollow(FOLLOW_instr_in_instr503);
							els=instr();
							state._fsp--;

							stream_instr.add(els.getTree());
							}
							break;

					}

					// AST REWRITE
					// elements: els, then, expr1
					// token labels: 
					// rule labels: then, els, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_then=new RewriteRuleSubtreeStream(adaptor,"rule then",then!=null?then.getTree():null);
					RewriteRuleSubtreeStream stream_els=new RewriteRuleSubtreeStream(adaptor,"rule els",els!=null?els.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 78:75: -> ^( IF expr1 ^( BODY $then) ( ^( ELSE $els) )? )
					{
						// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:78:78: ^( IF expr1 ^( BODY $then) ( ^( ELSE $els) )? )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(IF, "IF"), root_1);
						adaptor.addChild(root_1, stream_expr1.nextTree());
						// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:78:89: ^( BODY $then)
						{
						CommonTree root_2 = (CommonTree)adaptor.nil();
						root_2 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(BODY, "BODY"), root_2);
						adaptor.addChild(root_2, stream_then.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:78:103: ( ^( ELSE $els) )?
						if ( stream_els.hasNext() ) {
							// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:78:103: ^( ELSE $els)
							{
							CommonTree root_2 = (CommonTree)adaptor.nil();
							root_2 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(ELSE, "ELSE"), root_2);
							adaptor.addChild(root_2, stream_els.nextTree());
							adaptor.addChild(root_1, root_2);
							}

						}
						stream_els.reset();

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:79:4: 'while' expr1 'do' instr
					{
					string_literal51=(Token)match(input,66,FOLLOW_66_in_instr533);  
					stream_66.add(string_literal51);

					pushFollow(FOLLOW_expr1_in_instr535);
					expr152=expr1();
					state._fsp--;

					stream_expr1.add(expr152.getTree());
					string_literal53=(Token)match(input,51,FOLLOW_51_in_instr537);  
					stream_51.add(string_literal53);

					pushFollow(FOLLOW_instr_in_instr539);
					instr54=instr();
					state._fsp--;

					stream_instr.add(instr54.getTree());
					// AST REWRITE
					// elements: instr, expr1
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 79:29: -> ^( WHILE expr1 ^( BODY instr ) )
					{
						// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:79:32: ^( WHILE expr1 ^( BODY instr ) )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(WHILE, "WHILE"), root_1);
						adaptor.addChild(root_1, stream_expr1.nextTree());
						// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:79:46: ^( BODY instr )
						{
						CommonTree root_2 = (CommonTree)adaptor.nil();
						root_2 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(BODY, "BODY"), root_2);
						adaptor.addChild(root_2, stream_instr.nextTree());
						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 3 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:80:4: 'return' ( expr1 )?
					{
					string_literal55=(Token)match(input,62,FOLLOW_62_in_instr558);  
					stream_62.add(string_literal55);

					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:80:13: ( expr1 )?
					int alt11=2;
					int LA11_0 = input.LA(1);
					if ( (LA11_0==CSTE||LA11_0==IDF||LA11_0==29||LA11_0==34||LA11_0==56) ) {
						alt11=1;
					}
					switch (alt11) {
						case 1 :
							// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:80:13: expr1
							{
							pushFollow(FOLLOW_expr1_in_instr560);
							expr156=expr1();
							state._fsp--;

							stream_expr1.add(expr156.getTree());
							}
							break;

					}

					// AST REWRITE
					// elements: expr1
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 80:20: -> ^( RETURN expr1 )
					{
						// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:80:23: ^( RETURN expr1 )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(RETURN, "RETURN"), root_1);
						adaptor.addChild(root_1, stream_expr1.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 4 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:81:4: IDF ( '(' ( exprlist )? ')' -> ^( CALLFUNC IDF ( exprlist )? ) | ( '[' exprlist ']' '=' expr1 -> ^( AFFECT ^( ARRAYVAL IDF exprlist ) expr1 ) | '=' expr1 -> ^( AFFECT IDF expr1 ) ) )
					{
					IDF57=(Token)match(input,IDF,FOLLOW_IDF_in_instr574);  
					stream_IDF.add(IDF57);

					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:82:3: ( '(' ( exprlist )? ')' -> ^( CALLFUNC IDF ( exprlist )? ) | ( '[' exprlist ']' '=' expr1 -> ^( AFFECT ^( ARRAYVAL IDF exprlist ) expr1 ) | '=' expr1 -> ^( AFFECT IDF expr1 ) ) )
					int alt14=2;
					int LA14_0 = input.LA(1);
					if ( (LA14_0==29) ) {
						alt14=1;
					}
					else if ( (LA14_0==41||LA14_0==45) ) {
						alt14=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 14, 0, input);
						throw nvae;
					}

					switch (alt14) {
						case 1 :
							// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:82:5: '(' ( exprlist )? ')'
							{
							char_literal58=(Token)match(input,29,FOLLOW_29_in_instr581);  
							stream_29.add(char_literal58);

							// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:82:9: ( exprlist )?
							int alt12=2;
							int LA12_0 = input.LA(1);
							if ( (LA12_0==CSTE||LA12_0==IDF||LA12_0==29||LA12_0==34||LA12_0==56) ) {
								alt12=1;
							}
							switch (alt12) {
								case 1 :
									// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:82:9: exprlist
									{
									pushFollow(FOLLOW_exprlist_in_instr583);
									exprlist59=exprlist();
									state._fsp--;

									stream_exprlist.add(exprlist59.getTree());
									}
									break;

							}

							char_literal60=(Token)match(input,30,FOLLOW_30_in_instr586);  
							stream_30.add(char_literal60);

							// AST REWRITE
							// elements: exprlist, IDF
							// token labels: 
							// rule labels: retval
							// token list labels: 
							// rule list labels: 
							// wildcard labels: 
							retval.tree = root_0;
							RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

							root_0 = (CommonTree)adaptor.nil();
							// 82:23: -> ^( CALLFUNC IDF ( exprlist )? )
							{
								// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:82:26: ^( CALLFUNC IDF ( exprlist )? )
								{
								CommonTree root_1 = (CommonTree)adaptor.nil();
								root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(CALLFUNC, "CALLFUNC"), root_1);
								adaptor.addChild(root_1, stream_IDF.nextNode());
								// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:82:41: ( exprlist )?
								if ( stream_exprlist.hasNext() ) {
									adaptor.addChild(root_1, stream_exprlist.nextTree());
								}
								stream_exprlist.reset();

								adaptor.addChild(root_0, root_1);
								}

							}


							retval.tree = root_0;

							}
							break;
						case 2 :
							// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:83:5: ( '[' exprlist ']' '=' expr1 -> ^( AFFECT ^( ARRAYVAL IDF exprlist ) expr1 ) | '=' expr1 -> ^( AFFECT IDF expr1 ) )
							{
							// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:83:5: ( '[' exprlist ']' '=' expr1 -> ^( AFFECT ^( ARRAYVAL IDF exprlist ) expr1 ) | '=' expr1 -> ^( AFFECT IDF expr1 ) )
							int alt13=2;
							int LA13_0 = input.LA(1);
							if ( (LA13_0==45) ) {
								alt13=1;
							}
							else if ( (LA13_0==41) ) {
								alt13=2;
							}

							else {
								NoViableAltException nvae =
									new NoViableAltException("", 13, 0, input);
								throw nvae;
							}

							switch (alt13) {
								case 1 :
									// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:83:6: '[' exprlist ']' '=' expr1
									{
									char_literal61=(Token)match(input,45,FOLLOW_45_in_instr604);  
									stream_45.add(char_literal61);

									pushFollow(FOLLOW_exprlist_in_instr606);
									exprlist62=exprlist();
									state._fsp--;

									stream_exprlist.add(exprlist62.getTree());
									char_literal63=(Token)match(input,46,FOLLOW_46_in_instr608);  
									stream_46.add(char_literal63);

									char_literal64=(Token)match(input,41,FOLLOW_41_in_instr610);  
									stream_41.add(char_literal64);

									pushFollow(FOLLOW_expr1_in_instr612);
									expr165=expr1();
									state._fsp--;

									stream_expr1.add(expr165.getTree());
									// AST REWRITE
									// elements: IDF, expr1, exprlist
									// token labels: 
									// rule labels: retval
									// token list labels: 
									// rule list labels: 
									// wildcard labels: 
									retval.tree = root_0;
									RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

									root_0 = (CommonTree)adaptor.nil();
									// 83:33: -> ^( AFFECT ^( ARRAYVAL IDF exprlist ) expr1 )
									{
										// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:83:36: ^( AFFECT ^( ARRAYVAL IDF exprlist ) expr1 )
										{
										CommonTree root_1 = (CommonTree)adaptor.nil();
										root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(AFFECT, "AFFECT"), root_1);
										// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:83:45: ^( ARRAYVAL IDF exprlist )
										{
										CommonTree root_2 = (CommonTree)adaptor.nil();
										root_2 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(ARRAYVAL, "ARRAYVAL"), root_2);
										adaptor.addChild(root_2, stream_IDF.nextNode());
										adaptor.addChild(root_2, stream_exprlist.nextTree());
										adaptor.addChild(root_1, root_2);
										}

										adaptor.addChild(root_1, stream_expr1.nextTree());
										adaptor.addChild(root_0, root_1);
										}

									}


									retval.tree = root_0;

									}
									break;
								case 2 :
									// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:84:6: '=' expr1
									{
									char_literal66=(Token)match(input,41,FOLLOW_41_in_instr635);  
									stream_41.add(char_literal66);

									pushFollow(FOLLOW_expr1_in_instr637);
									expr167=expr1();
									state._fsp--;

									stream_expr1.add(expr167.getTree());
									// AST REWRITE
									// elements: IDF, expr1
									// token labels: 
									// rule labels: retval
									// token list labels: 
									// rule list labels: 
									// wildcard labels: 
									retval.tree = root_0;
									RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

									root_0 = (CommonTree)adaptor.nil();
									// 84:16: -> ^( AFFECT IDF expr1 )
									{
										// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:84:19: ^( AFFECT IDF expr1 )
										{
										CommonTree root_1 = (CommonTree)adaptor.nil();
										root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(AFFECT, "AFFECT"), root_1);
										adaptor.addChild(root_1, stream_IDF.nextNode());
										adaptor.addChild(root_1, stream_expr1.nextTree());
										adaptor.addChild(root_0, root_1);
										}

									}


									retval.tree = root_0;

									}
									break;

							}

							}
							break;

					}

					}
					break;
				case 5 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:87:4: '{' ( sequence )? '}'
					{
					char_literal68=(Token)match(input,68,FOLLOW_68_in_instr661);  
					stream_68.add(char_literal68);

					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:87:8: ( sequence )?
					int alt15=2;
					int LA15_0 = input.LA(1);
					if ( (LA15_0==IDF||LA15_0==54||LA15_0==60||LA15_0==62||(LA15_0 >= 66 && LA15_0 <= 68)) ) {
						alt15=1;
					}
					switch (alt15) {
						case 1 :
							// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:87:8: sequence
							{
							pushFollow(FOLLOW_sequence_in_instr663);
							sequence69=sequence();
							state._fsp--;

							stream_sequence.add(sequence69.getTree());
							}
							break;

					}

					char_literal70=(Token)match(input,69,FOLLOW_69_in_instr666);  
					stream_69.add(char_literal70);

					// AST REWRITE
					// elements: sequence
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 87:22: -> ( sequence )?
					{
						// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:87:25: ( sequence )?
						if ( stream_sequence.hasNext() ) {
							adaptor.addChild(root_0, stream_sequence.nextTree());
						}
						stream_sequence.reset();

					}


					retval.tree = root_0;

					}
					break;
				case 6 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:88:4: 'read' lvalue
					{
					string_literal71=(Token)match(input,60,FOLLOW_60_in_instr676);  
					stream_60.add(string_literal71);

					pushFollow(FOLLOW_lvalue_in_instr678);
					lvalue72=lvalue();
					state._fsp--;

					stream_lvalue.add(lvalue72.getTree());
					// AST REWRITE
					// elements: lvalue
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 88:18: -> ^( READ lvalue )
					{
						// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:88:21: ^( READ lvalue )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(READ, "READ"), root_1);
						adaptor.addChild(root_1, stream_lvalue.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 7 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:89:4: 'write' ( lvalue | CSTE )
					{
					string_literal73=(Token)match(input,67,FOLLOW_67_in_instr691);  
					stream_67.add(string_literal73);

					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:89:12: ( lvalue | CSTE )
					int alt16=2;
					int LA16_0 = input.LA(1);
					if ( (LA16_0==IDF) ) {
						alt16=1;
					}
					else if ( (LA16_0==CSTE) ) {
						alt16=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 16, 0, input);
						throw nvae;
					}

					switch (alt16) {
						case 1 :
							// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:89:13: lvalue
							{
							pushFollow(FOLLOW_lvalue_in_instr694);
							lvalue74=lvalue();
							state._fsp--;

							stream_lvalue.add(lvalue74.getTree());
							}
							break;
						case 2 :
							// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:89:20: CSTE
							{
							CSTE75=(Token)match(input,CSTE,FOLLOW_CSTE_in_instr696);  
							stream_CSTE.add(CSTE75);

							}
							break;

					}

					// AST REWRITE
					// elements: lvalue, CSTE
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 89:26: -> ^( WRITE ( lvalue )? ( CSTE )? )
					{
						// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:89:29: ^( WRITE ( lvalue )? ( CSTE )? )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(WRITE, "WRITE"), root_1);
						// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:89:37: ( lvalue )?
						if ( stream_lvalue.hasNext() ) {
							adaptor.addChild(root_1, stream_lvalue.nextTree());
						}
						stream_lvalue.reset();

						// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:89:45: ( CSTE )?
						if ( stream_CSTE.hasNext() ) {
							adaptor.addChild(root_1, stream_CSTE.nextNode());
						}
						stream_CSTE.reset();

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "instr"


	public static class sequence_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "sequence"
	// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:92:1: sequence : instr ( ';' ( sequence )? )? -> instr ( sequence )? ;
	public final ProjetParser.sequence_return sequence() throws RecognitionException {
		ProjetParser.sequence_return retval = new ProjetParser.sequence_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token char_literal77=null;
		ParserRuleReturnScope instr76 =null;
		ParserRuleReturnScope sequence78 =null;

		CommonTree char_literal77_tree=null;
		RewriteRuleTokenStream stream_38=new RewriteRuleTokenStream(adaptor,"token 38");
		RewriteRuleSubtreeStream stream_sequence=new RewriteRuleSubtreeStream(adaptor,"rule sequence");
		RewriteRuleSubtreeStream stream_instr=new RewriteRuleSubtreeStream(adaptor,"rule instr");

		try {
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:93:2: ( instr ( ';' ( sequence )? )? -> instr ( sequence )? )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:93:4: instr ( ';' ( sequence )? )?
			{
			pushFollow(FOLLOW_instr_in_sequence721);
			instr76=instr();
			state._fsp--;

			stream_instr.add(instr76.getTree());
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:93:10: ( ';' ( sequence )? )?
			int alt19=2;
			int LA19_0 = input.LA(1);
			if ( (LA19_0==38) ) {
				alt19=1;
			}
			switch (alt19) {
				case 1 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:93:11: ';' ( sequence )?
					{
					char_literal77=(Token)match(input,38,FOLLOW_38_in_sequence724);  
					stream_38.add(char_literal77);

					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:93:15: ( sequence )?
					int alt18=2;
					int LA18_0 = input.LA(1);
					if ( (LA18_0==IDF||LA18_0==54||LA18_0==60||LA18_0==62||(LA18_0 >= 66 && LA18_0 <= 68)) ) {
						alt18=1;
					}
					switch (alt18) {
						case 1 :
							// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:93:15: sequence
							{
							pushFollow(FOLLOW_sequence_in_sequence726);
							sequence78=sequence();
							state._fsp--;

							stream_sequence.add(sequence78.getTree());
							}
							break;

					}

					}
					break;

			}

			// AST REWRITE
			// elements: sequence, instr
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 93:27: -> instr ( sequence )?
			{
				adaptor.addChild(root_0, stream_instr.nextTree());
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:93:36: ( sequence )?
				if ( stream_sequence.hasNext() ) {
					adaptor.addChild(root_0, stream_sequence.nextTree());
				}
				stream_sequence.reset();

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "sequence"


	public static class exprlist_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "exprlist"
	// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:96:1: exprlist : expr1 ( ',' exprlist )? -> expr1 ( exprlist )? ;
	public final ProjetParser.exprlist_return exprlist() throws RecognitionException {
		ProjetParser.exprlist_return retval = new ProjetParser.exprlist_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token char_literal80=null;
		ParserRuleReturnScope expr179 =null;
		ParserRuleReturnScope exprlist81 =null;

		CommonTree char_literal80_tree=null;
		RewriteRuleTokenStream stream_33=new RewriteRuleTokenStream(adaptor,"token 33");
		RewriteRuleSubtreeStream stream_exprlist=new RewriteRuleSubtreeStream(adaptor,"rule exprlist");
		RewriteRuleSubtreeStream stream_expr1=new RewriteRuleSubtreeStream(adaptor,"rule expr1");

		try {
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:97:2: ( expr1 ( ',' exprlist )? -> expr1 ( exprlist )? )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:97:4: expr1 ( ',' exprlist )?
			{
			pushFollow(FOLLOW_expr1_in_exprlist748);
			expr179=expr1();
			state._fsp--;

			stream_expr1.add(expr179.getTree());
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:97:10: ( ',' exprlist )?
			int alt20=2;
			int LA20_0 = input.LA(1);
			if ( (LA20_0==33) ) {
				alt20=1;
			}
			switch (alt20) {
				case 1 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:97:11: ',' exprlist
					{
					char_literal80=(Token)match(input,33,FOLLOW_33_in_exprlist751);  
					stream_33.add(char_literal80);

					pushFollow(FOLLOW_exprlist_in_exprlist753);
					exprlist81=exprlist();
					state._fsp--;

					stream_exprlist.add(exprlist81.getTree());
					}
					break;

			}

			// AST REWRITE
			// elements: exprlist, expr1
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 97:26: -> expr1 ( exprlist )?
			{
				adaptor.addChild(root_0, stream_expr1.nextTree());
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:97:35: ( exprlist )?
				if ( stream_exprlist.hasNext() ) {
					adaptor.addChild(root_0, stream_exprlist.nextTree());
				}
				stream_exprlist.reset();

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "exprlist"


	public static class lvalue_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "lvalue"
	// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:100:1: lvalue : IDF ( '[' exprlist ']' -> ^( ARRAYVAL IDF exprlist ) | -> IDF ) ;
	public final ProjetParser.lvalue_return lvalue() throws RecognitionException {
		ProjetParser.lvalue_return retval = new ProjetParser.lvalue_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token IDF82=null;
		Token char_literal83=null;
		Token char_literal85=null;
		ParserRuleReturnScope exprlist84 =null;

		CommonTree IDF82_tree=null;
		CommonTree char_literal83_tree=null;
		CommonTree char_literal85_tree=null;
		RewriteRuleTokenStream stream_45=new RewriteRuleTokenStream(adaptor,"token 45");
		RewriteRuleTokenStream stream_46=new RewriteRuleTokenStream(adaptor,"token 46");
		RewriteRuleTokenStream stream_IDF=new RewriteRuleTokenStream(adaptor,"token IDF");
		RewriteRuleSubtreeStream stream_exprlist=new RewriteRuleSubtreeStream(adaptor,"rule exprlist");

		try {
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:101:2: ( IDF ( '[' exprlist ']' -> ^( ARRAYVAL IDF exprlist ) | -> IDF ) )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:101:4: IDF ( '[' exprlist ']' -> ^( ARRAYVAL IDF exprlist ) | -> IDF )
			{
			IDF82=(Token)match(input,IDF,FOLLOW_IDF_in_lvalue774);  
			stream_IDF.add(IDF82);

			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:102:3: ( '[' exprlist ']' -> ^( ARRAYVAL IDF exprlist ) | -> IDF )
			int alt21=2;
			int LA21_0 = input.LA(1);
			if ( (LA21_0==45) ) {
				alt21=1;
			}
			else if ( (LA21_0==EOF||LA21_0==38||LA21_0==52||LA21_0==69) ) {
				alt21=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 21, 0, input);
				throw nvae;
			}

			switch (alt21) {
				case 1 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:102:4: '[' exprlist ']'
					{
					char_literal83=(Token)match(input,45,FOLLOW_45_in_lvalue779);  
					stream_45.add(char_literal83);

					pushFollow(FOLLOW_exprlist_in_lvalue781);
					exprlist84=exprlist();
					state._fsp--;

					stream_exprlist.add(exprlist84.getTree());
					char_literal85=(Token)match(input,46,FOLLOW_46_in_lvalue783);  
					stream_46.add(char_literal85);

					// AST REWRITE
					// elements: IDF, exprlist
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 102:20: -> ^( ARRAYVAL IDF exprlist )
					{
						// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:102:23: ^( ARRAYVAL IDF exprlist )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(ARRAYVAL, "ARRAYVAL"), root_1);
						adaptor.addChild(root_1, stream_IDF.nextNode());
						adaptor.addChild(root_1, stream_exprlist.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:103:5: 
					{
					// AST REWRITE
					// elements: IDF
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 103:5: -> IDF
					{
						adaptor.addChild(root_0, stream_IDF.nextNode());
					}


					retval.tree = root_0;

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "lvalue"


	public static class expr1_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "expr1"
	// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:107:1: expr1 : expr2 ( -> expr2 | 'or' expr1 -> ^( 'or' expr2 expr1 ) ) ;
	public final ProjetParser.expr1_return expr1() throws RecognitionException {
		ProjetParser.expr1_return retval = new ProjetParser.expr1_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token string_literal87=null;
		ParserRuleReturnScope expr286 =null;
		ParserRuleReturnScope expr188 =null;

		CommonTree string_literal87_tree=null;
		RewriteRuleTokenStream stream_58=new RewriteRuleTokenStream(adaptor,"token 58");
		RewriteRuleSubtreeStream stream_expr2=new RewriteRuleSubtreeStream(adaptor,"rule expr2");
		RewriteRuleSubtreeStream stream_expr1=new RewriteRuleSubtreeStream(adaptor,"rule expr1");

		try {
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:107:7: ( expr2 ( -> expr2 | 'or' expr1 -> ^( 'or' expr2 expr1 ) ) )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:107:9: expr2 ( -> expr2 | 'or' expr1 -> ^( 'or' expr2 expr1 ) )
			{
			pushFollow(FOLLOW_expr2_in_expr1818);
			expr286=expr2();
			state._fsp--;

			stream_expr2.add(expr286.getTree());
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:107:15: ( -> expr2 | 'or' expr1 -> ^( 'or' expr2 expr1 ) )
			int alt22=2;
			int LA22_0 = input.LA(1);
			if ( (LA22_0==EOF||LA22_0==30||LA22_0==33||LA22_0==38||LA22_0==46||(LA22_0 >= 51 && LA22_0 <= 52)||LA22_0==63||LA22_0==69) ) {
				alt22=1;
			}
			else if ( (LA22_0==58) ) {
				alt22=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 22, 0, input);
				throw nvae;
			}

			switch (alt22) {
				case 1 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:107:16: 
					{
					// AST REWRITE
					// elements: expr2
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 107:16: -> expr2
					{
						adaptor.addChild(root_0, stream_expr2.nextTree());
					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:107:24: 'or' expr1
					{
					string_literal87=(Token)match(input,58,FOLLOW_58_in_expr1824);  
					stream_58.add(string_literal87);

					pushFollow(FOLLOW_expr1_in_expr1826);
					expr188=expr1();
					state._fsp--;

					stream_expr1.add(expr188.getTree());
					// AST REWRITE
					// elements: expr2, 58, expr1
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 107:35: -> ^( 'or' expr2 expr1 )
					{
						// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:107:38: ^( 'or' expr2 expr1 )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot(stream_58.nextNode(), root_1);
						adaptor.addChild(root_1, stream_expr2.nextTree());
						adaptor.addChild(root_1, stream_expr1.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "expr1"


	public static class expr2_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "expr2"
	// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:109:1: expr2 : expr3 ( -> expr3 | 'and' expr2 -> ^( 'and' expr3 expr2 ) ) ;
	public final ProjetParser.expr2_return expr2() throws RecognitionException {
		ProjetParser.expr2_return retval = new ProjetParser.expr2_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token string_literal90=null;
		ParserRuleReturnScope expr389 =null;
		ParserRuleReturnScope expr291 =null;

		CommonTree string_literal90_tree=null;
		RewriteRuleTokenStream stream_48=new RewriteRuleTokenStream(adaptor,"token 48");
		RewriteRuleSubtreeStream stream_expr3=new RewriteRuleSubtreeStream(adaptor,"rule expr3");
		RewriteRuleSubtreeStream stream_expr2=new RewriteRuleSubtreeStream(adaptor,"rule expr2");

		try {
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:109:7: ( expr3 ( -> expr3 | 'and' expr2 -> ^( 'and' expr3 expr2 ) ) )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:109:9: expr3 ( -> expr3 | 'and' expr2 -> ^( 'and' expr3 expr2 ) )
			{
			pushFollow(FOLLOW_expr3_in_expr2846);
			expr389=expr3();
			state._fsp--;

			stream_expr3.add(expr389.getTree());
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:109:15: ( -> expr3 | 'and' expr2 -> ^( 'and' expr3 expr2 ) )
			int alt23=2;
			int LA23_0 = input.LA(1);
			if ( (LA23_0==EOF||LA23_0==30||LA23_0==33||LA23_0==38||LA23_0==46||(LA23_0 >= 51 && LA23_0 <= 52)||LA23_0==58||LA23_0==63||LA23_0==69) ) {
				alt23=1;
			}
			else if ( (LA23_0==48) ) {
				alt23=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 23, 0, input);
				throw nvae;
			}

			switch (alt23) {
				case 1 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:109:16: 
					{
					// AST REWRITE
					// elements: expr3
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 109:16: -> expr3
					{
						adaptor.addChild(root_0, stream_expr3.nextTree());
					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:109:24: 'and' expr2
					{
					string_literal90=(Token)match(input,48,FOLLOW_48_in_expr2852);  
					stream_48.add(string_literal90);

					pushFollow(FOLLOW_expr2_in_expr2854);
					expr291=expr2();
					state._fsp--;

					stream_expr2.add(expr291.getTree());
					// AST REWRITE
					// elements: expr2, 48, expr3
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 109:36: -> ^( 'and' expr3 expr2 )
					{
						// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:109:39: ^( 'and' expr3 expr2 )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot(stream_48.nextNode(), root_1);
						adaptor.addChild(root_1, stream_expr3.nextTree());
						adaptor.addChild(root_1, stream_expr2.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "expr2"


	public static class expr3_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "expr3"
	// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:111:1: expr3 : expr4 ( -> expr4 | operator expr3 -> ^( operator expr4 expr3 ) ) ;
	public final ProjetParser.expr3_return expr3() throws RecognitionException {
		ProjetParser.expr3_return retval = new ProjetParser.expr3_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		ParserRuleReturnScope expr492 =null;
		ParserRuleReturnScope operator93 =null;
		ParserRuleReturnScope expr394 =null;

		RewriteRuleSubtreeStream stream_expr4=new RewriteRuleSubtreeStream(adaptor,"rule expr4");
		RewriteRuleSubtreeStream stream_expr3=new RewriteRuleSubtreeStream(adaptor,"rule expr3");
		RewriteRuleSubtreeStream stream_operator=new RewriteRuleSubtreeStream(adaptor,"rule operator");

		try {
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:111:7: ( expr4 ( -> expr4 | operator expr3 -> ^( operator expr4 expr3 ) ) )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:111:9: expr4 ( -> expr4 | operator expr3 -> ^( operator expr4 expr3 ) )
			{
			pushFollow(FOLLOW_expr4_in_expr3874);
			expr492=expr4();
			state._fsp--;

			stream_expr4.add(expr492.getTree());
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:111:15: ( -> expr4 | operator expr3 -> ^( operator expr4 expr3 ) )
			int alt24=2;
			int LA24_0 = input.LA(1);
			if ( (LA24_0==EOF||LA24_0==30||LA24_0==33||LA24_0==38||LA24_0==46||LA24_0==48||(LA24_0 >= 51 && LA24_0 <= 52)||LA24_0==58||LA24_0==63||LA24_0==69) ) {
				alt24=1;
			}
			else if ( (LA24_0==28||(LA24_0 >= 39 && LA24_0 <= 40)||(LA24_0 >= 42 && LA24_0 <= 44)) ) {
				alt24=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 24, 0, input);
				throw nvae;
			}

			switch (alt24) {
				case 1 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:111:16: 
					{
					// AST REWRITE
					// elements: expr4
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 111:16: -> expr4
					{
						adaptor.addChild(root_0, stream_expr4.nextTree());
					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:111:24: operator expr3
					{
					pushFollow(FOLLOW_operator_in_expr3880);
					operator93=operator();
					state._fsp--;

					stream_operator.add(operator93.getTree());
					pushFollow(FOLLOW_expr3_in_expr3882);
					expr394=expr3();
					state._fsp--;

					stream_expr3.add(expr394.getTree());
					// AST REWRITE
					// elements: operator, expr3, expr4
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 111:39: -> ^( operator expr4 expr3 )
					{
						// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:111:42: ^( operator expr4 expr3 )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot(stream_operator.nextNode(), root_1);
						adaptor.addChild(root_1, stream_expr4.nextTree());
						adaptor.addChild(root_1, stream_expr3.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "expr3"


	public static class operator_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "operator"
	// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:112:1: operator : ( '==' | '<=' | '>=' | '<' | '>' | '!=' );
	public final ProjetParser.operator_return operator() throws RecognitionException {
		ProjetParser.operator_return retval = new ProjetParser.operator_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token set95=null;

		CommonTree set95_tree=null;

		try {
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:112:10: ( '==' | '<=' | '>=' | '<' | '>' | '!=' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:
			{
			root_0 = (CommonTree)adaptor.nil();


			set95=input.LT(1);
			if ( input.LA(1)==28||(input.LA(1) >= 39 && input.LA(1) <= 40)||(input.LA(1) >= 42 && input.LA(1) <= 44) ) {
				input.consume();
				adaptor.addChild(root_0, (CommonTree)adaptor.create(set95));
				state.errorRecovery=false;
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "operator"


	public static class expr4_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "expr4"
	// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:114:1: expr4 : expr5 ( -> expr5 | plusMoins expr4 -> ^( plusMoins expr5 expr4 ) ) ;
	public final ProjetParser.expr4_return expr4() throws RecognitionException {
		ProjetParser.expr4_return retval = new ProjetParser.expr4_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		ParserRuleReturnScope expr596 =null;
		ParserRuleReturnScope plusMoins97 =null;
		ParserRuleReturnScope expr498 =null;

		RewriteRuleSubtreeStream stream_plusMoins=new RewriteRuleSubtreeStream(adaptor,"rule plusMoins");
		RewriteRuleSubtreeStream stream_expr5=new RewriteRuleSubtreeStream(adaptor,"rule expr5");
		RewriteRuleSubtreeStream stream_expr4=new RewriteRuleSubtreeStream(adaptor,"rule expr4");

		try {
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:114:7: ( expr5 ( -> expr5 | plusMoins expr4 -> ^( plusMoins expr5 expr4 ) ) )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:114:9: expr5 ( -> expr5 | plusMoins expr4 -> ^( plusMoins expr5 expr4 ) )
			{
			pushFollow(FOLLOW_expr5_in_expr4928);
			expr596=expr5();
			state._fsp--;

			stream_expr5.add(expr596.getTree());
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:114:15: ( -> expr5 | plusMoins expr4 -> ^( plusMoins expr5 expr4 ) )
			int alt25=2;
			int LA25_0 = input.LA(1);
			if ( (LA25_0==EOF||LA25_0==28||LA25_0==30||LA25_0==33||(LA25_0 >= 38 && LA25_0 <= 40)||(LA25_0 >= 42 && LA25_0 <= 44)||LA25_0==46||LA25_0==48||(LA25_0 >= 51 && LA25_0 <= 52)||LA25_0==58||LA25_0==63||LA25_0==69) ) {
				alt25=1;
			}
			else if ( (LA25_0==32||LA25_0==34) ) {
				alt25=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 25, 0, input);
				throw nvae;
			}

			switch (alt25) {
				case 1 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:114:16: 
					{
					// AST REWRITE
					// elements: expr5
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 114:16: -> expr5
					{
						adaptor.addChild(root_0, stream_expr5.nextTree());
					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:114:24: plusMoins expr4
					{
					pushFollow(FOLLOW_plusMoins_in_expr4934);
					plusMoins97=plusMoins();
					state._fsp--;

					stream_plusMoins.add(plusMoins97.getTree());
					pushFollow(FOLLOW_expr4_in_expr4936);
					expr498=expr4();
					state._fsp--;

					stream_expr4.add(expr498.getTree());
					// AST REWRITE
					// elements: expr4, expr5, plusMoins
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 114:40: -> ^( plusMoins expr5 expr4 )
					{
						// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:114:43: ^( plusMoins expr5 expr4 )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot(stream_plusMoins.nextNode(), root_1);
						adaptor.addChild(root_1, stream_expr5.nextTree());
						adaptor.addChild(root_1, stream_expr4.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "expr4"


	public static class plusMoins_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "plusMoins"
	// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:115:1: plusMoins : ( '+' | '-' );
	public final ProjetParser.plusMoins_return plusMoins() throws RecognitionException {
		ProjetParser.plusMoins_return retval = new ProjetParser.plusMoins_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token set99=null;

		CommonTree set99_tree=null;

		try {
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:115:11: ( '+' | '-' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:
			{
			root_0 = (CommonTree)adaptor.nil();


			set99=input.LT(1);
			if ( input.LA(1)==32||input.LA(1)==34 ) {
				input.consume();
				adaptor.addChild(root_0, (CommonTree)adaptor.create(set99));
				state.errorRecovery=false;
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "plusMoins"


	public static class expr5_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "expr5"
	// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:117:1: expr5 : expr6 ( -> expr6 | multDiv expr5 -> ^( multDiv expr6 expr5 ) ) ;
	public final ProjetParser.expr5_return expr5() throws RecognitionException {
		ProjetParser.expr5_return retval = new ProjetParser.expr5_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		ParserRuleReturnScope expr6100 =null;
		ParserRuleReturnScope multDiv101 =null;
		ParserRuleReturnScope expr5102 =null;

		RewriteRuleSubtreeStream stream_multDiv=new RewriteRuleSubtreeStream(adaptor,"rule multDiv");
		RewriteRuleSubtreeStream stream_expr6=new RewriteRuleSubtreeStream(adaptor,"rule expr6");
		RewriteRuleSubtreeStream stream_expr5=new RewriteRuleSubtreeStream(adaptor,"rule expr5");

		try {
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:117:7: ( expr6 ( -> expr6 | multDiv expr5 -> ^( multDiv expr6 expr5 ) ) )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:117:9: expr6 ( -> expr6 | multDiv expr5 -> ^( multDiv expr6 expr5 ) )
			{
			pushFollow(FOLLOW_expr6_in_expr5966);
			expr6100=expr6();
			state._fsp--;

			stream_expr6.add(expr6100.getTree());
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:117:15: ( -> expr6 | multDiv expr5 -> ^( multDiv expr6 expr5 ) )
			int alt26=2;
			int LA26_0 = input.LA(1);
			if ( (LA26_0==EOF||LA26_0==28||LA26_0==30||(LA26_0 >= 32 && LA26_0 <= 34)||(LA26_0 >= 38 && LA26_0 <= 40)||(LA26_0 >= 42 && LA26_0 <= 44)||LA26_0==46||LA26_0==48||(LA26_0 >= 51 && LA26_0 <= 52)||LA26_0==58||LA26_0==63||LA26_0==69) ) {
				alt26=1;
			}
			else if ( (LA26_0==31||LA26_0==36) ) {
				alt26=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 26, 0, input);
				throw nvae;
			}

			switch (alt26) {
				case 1 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:117:16: 
					{
					// AST REWRITE
					// elements: expr6
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 117:16: -> expr6
					{
						adaptor.addChild(root_0, stream_expr6.nextTree());
					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:117:24: multDiv expr5
					{
					pushFollow(FOLLOW_multDiv_in_expr5972);
					multDiv101=multDiv();
					state._fsp--;

					stream_multDiv.add(multDiv101.getTree());
					pushFollow(FOLLOW_expr5_in_expr5974);
					expr5102=expr5();
					state._fsp--;

					stream_expr5.add(expr5102.getTree());
					// AST REWRITE
					// elements: multDiv, expr5, expr6
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 117:38: -> ^( multDiv expr6 expr5 )
					{
						// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:117:41: ^( multDiv expr6 expr5 )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot(stream_multDiv.nextNode(), root_1);
						adaptor.addChild(root_1, stream_expr6.nextTree());
						adaptor.addChild(root_1, stream_expr5.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "expr5"


	public static class multDiv_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "multDiv"
	// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:118:1: multDiv : ( '*' | '/' );
	public final ProjetParser.multDiv_return multDiv() throws RecognitionException {
		ProjetParser.multDiv_return retval = new ProjetParser.multDiv_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token set103=null;

		CommonTree set103_tree=null;

		try {
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:118:9: ( '*' | '/' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:
			{
			root_0 = (CommonTree)adaptor.nil();


			set103=input.LT(1);
			if ( input.LA(1)==31||input.LA(1)==36 ) {
				input.consume();
				adaptor.addChild(root_0, (CommonTree)adaptor.create(set103));
				state.errorRecovery=false;
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "multDiv"


	public static class expr6_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "expr6"
	// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:120:1: expr6 : ( expr7 -> expr7 | opun expr7 -> ^( opun expr7 ) );
	public final ProjetParser.expr6_return expr6() throws RecognitionException {
		ProjetParser.expr6_return retval = new ProjetParser.expr6_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		ParserRuleReturnScope expr7104 =null;
		ParserRuleReturnScope opun105 =null;
		ParserRuleReturnScope expr7106 =null;

		RewriteRuleSubtreeStream stream_expr7=new RewriteRuleSubtreeStream(adaptor,"rule expr7");
		RewriteRuleSubtreeStream stream_opun=new RewriteRuleSubtreeStream(adaptor,"rule opun");

		try {
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:120:7: ( expr7 -> expr7 | opun expr7 -> ^( opun expr7 ) )
			int alt27=2;
			int LA27_0 = input.LA(1);
			if ( (LA27_0==CSTE||LA27_0==IDF||LA27_0==29) ) {
				alt27=1;
			}
			else if ( (LA27_0==34||LA27_0==56) ) {
				alt27=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 27, 0, input);
				throw nvae;
			}

			switch (alt27) {
				case 1 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:120:9: expr7
					{
					pushFollow(FOLLOW_expr7_in_expr61005);
					expr7104=expr7();
					state._fsp--;

					stream_expr7.add(expr7104.getTree());
					// AST REWRITE
					// elements: expr7
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 120:15: -> expr7
					{
						adaptor.addChild(root_0, stream_expr7.nextTree());
					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:120:26: opun expr7
					{
					pushFollow(FOLLOW_opun_in_expr61013);
					opun105=opun();
					state._fsp--;

					stream_opun.add(opun105.getTree());
					pushFollow(FOLLOW_expr7_in_expr61015);
					expr7106=expr7();
					state._fsp--;

					stream_expr7.add(expr7106.getTree());
					// AST REWRITE
					// elements: opun, expr7
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 120:37: -> ^( opun expr7 )
					{
						// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:120:40: ^( opun expr7 )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot(stream_opun.nextNode(), root_1);
						adaptor.addChild(root_1, stream_expr7.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "expr6"


	public static class opun_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "opun"
	// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:121:1: opun : ( '-' | 'not' );
	public final ProjetParser.opun_return opun() throws RecognitionException {
		ProjetParser.opun_return retval = new ProjetParser.opun_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token set107=null;

		CommonTree set107_tree=null;

		try {
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:121:6: ( '-' | 'not' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:
			{
			root_0 = (CommonTree)adaptor.nil();


			set107=input.LT(1);
			if ( input.LA(1)==34||input.LA(1)==56 ) {
				input.consume();
				adaptor.addChild(root_0, (CommonTree)adaptor.create(set107));
				state.errorRecovery=false;
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "opun"


	public static class expr7_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "expr7"
	// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:123:1: expr7 : expr8 ( -> expr8 | ( '^' expr7 ) -> ^( '^' expr8 expr7 ) ) ;
	public final ProjetParser.expr7_return expr7() throws RecognitionException {
		ProjetParser.expr7_return retval = new ProjetParser.expr7_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token char_literal109=null;
		ParserRuleReturnScope expr8108 =null;
		ParserRuleReturnScope expr7110 =null;

		CommonTree char_literal109_tree=null;
		RewriteRuleTokenStream stream_47=new RewriteRuleTokenStream(adaptor,"token 47");
		RewriteRuleSubtreeStream stream_expr8=new RewriteRuleSubtreeStream(adaptor,"rule expr8");
		RewriteRuleSubtreeStream stream_expr7=new RewriteRuleSubtreeStream(adaptor,"rule expr7");

		try {
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:123:7: ( expr8 ( -> expr8 | ( '^' expr7 ) -> ^( '^' expr8 expr7 ) ) )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:123:9: expr8 ( -> expr8 | ( '^' expr7 ) -> ^( '^' expr8 expr7 ) )
			{
			pushFollow(FOLLOW_expr8_in_expr71042);
			expr8108=expr8();
			state._fsp--;

			stream_expr8.add(expr8108.getTree());
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:123:15: ( -> expr8 | ( '^' expr7 ) -> ^( '^' expr8 expr7 ) )
			int alt28=2;
			int LA28_0 = input.LA(1);
			if ( (LA28_0==EOF||LA28_0==28||(LA28_0 >= 30 && LA28_0 <= 34)||LA28_0==36||(LA28_0 >= 38 && LA28_0 <= 40)||(LA28_0 >= 42 && LA28_0 <= 44)||LA28_0==46||LA28_0==48||(LA28_0 >= 51 && LA28_0 <= 52)||LA28_0==58||LA28_0==63||LA28_0==69) ) {
				alt28=1;
			}
			else if ( (LA28_0==47) ) {
				alt28=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 28, 0, input);
				throw nvae;
			}

			switch (alt28) {
				case 1 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:123:16: 
					{
					// AST REWRITE
					// elements: expr8
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 123:16: -> expr8
					{
						adaptor.addChild(root_0, stream_expr8.nextTree());
					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:123:24: ( '^' expr7 )
					{
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:123:24: ( '^' expr7 )
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:123:25: '^' expr7
					{
					char_literal109=(Token)match(input,47,FOLLOW_47_in_expr71049);  
					stream_47.add(char_literal109);

					pushFollow(FOLLOW_expr7_in_expr71051);
					expr7110=expr7();
					state._fsp--;

					stream_expr7.add(expr7110.getTree());
					}

					// AST REWRITE
					// elements: expr7, 47, expr8
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 123:36: -> ^( '^' expr8 expr7 )
					{
						// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:123:39: ^( '^' expr8 expr7 )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot(stream_47.nextNode(), root_1);
						adaptor.addChild(root_1, stream_expr8.nextTree());
						adaptor.addChild(root_1, stream_expr7.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "expr7"


	public static class expr8_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "expr8"
	// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:125:1: expr8 : ( CSTE -> CSTE | '(' expr1 ')' -> expr1 | IDF ( '(' ( exprlist )? ')' -> ^( CALLFUNC IDF ( exprlist )? ) | '[' exprlist ']' -> ^( ARRAYVAL IDF exprlist ) | -> IDF ) );
	public final ProjetParser.expr8_return expr8() throws RecognitionException {
		ProjetParser.expr8_return retval = new ProjetParser.expr8_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token CSTE111=null;
		Token char_literal112=null;
		Token char_literal114=null;
		Token IDF115=null;
		Token char_literal116=null;
		Token char_literal118=null;
		Token char_literal119=null;
		Token char_literal121=null;
		ParserRuleReturnScope expr1113 =null;
		ParserRuleReturnScope exprlist117 =null;
		ParserRuleReturnScope exprlist120 =null;

		CommonTree CSTE111_tree=null;
		CommonTree char_literal112_tree=null;
		CommonTree char_literal114_tree=null;
		CommonTree IDF115_tree=null;
		CommonTree char_literal116_tree=null;
		CommonTree char_literal118_tree=null;
		CommonTree char_literal119_tree=null;
		CommonTree char_literal121_tree=null;
		RewriteRuleTokenStream stream_45=new RewriteRuleTokenStream(adaptor,"token 45");
		RewriteRuleTokenStream stream_46=new RewriteRuleTokenStream(adaptor,"token 46");
		RewriteRuleTokenStream stream_29=new RewriteRuleTokenStream(adaptor,"token 29");
		RewriteRuleTokenStream stream_IDF=new RewriteRuleTokenStream(adaptor,"token IDF");
		RewriteRuleTokenStream stream_30=new RewriteRuleTokenStream(adaptor,"token 30");
		RewriteRuleTokenStream stream_CSTE=new RewriteRuleTokenStream(adaptor,"token CSTE");
		RewriteRuleSubtreeStream stream_exprlist=new RewriteRuleSubtreeStream(adaptor,"rule exprlist");
		RewriteRuleSubtreeStream stream_expr1=new RewriteRuleSubtreeStream(adaptor,"rule expr1");

		try {
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:126:2: ( CSTE -> CSTE | '(' expr1 ')' -> expr1 | IDF ( '(' ( exprlist )? ')' -> ^( CALLFUNC IDF ( exprlist )? ) | '[' exprlist ']' -> ^( ARRAYVAL IDF exprlist ) | -> IDF ) )
			int alt31=3;
			switch ( input.LA(1) ) {
			case CSTE:
				{
				alt31=1;
				}
				break;
			case 29:
				{
				alt31=2;
				}
				break;
			case IDF:
				{
				alt31=3;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 31, 0, input);
				throw nvae;
			}
			switch (alt31) {
				case 1 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:126:4: CSTE
					{
					CSTE111=(Token)match(input,CSTE,FOLLOW_CSTE_in_expr81074);  
					stream_CSTE.add(CSTE111);

					// AST REWRITE
					// elements: CSTE
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 126:9: -> CSTE
					{
						adaptor.addChild(root_0, stream_CSTE.nextNode());
					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:127:4: '(' expr1 ')'
					{
					char_literal112=(Token)match(input,29,FOLLOW_29_in_expr81083);  
					stream_29.add(char_literal112);

					pushFollow(FOLLOW_expr1_in_expr81085);
					expr1113=expr1();
					state._fsp--;

					stream_expr1.add(expr1113.getTree());
					char_literal114=(Token)match(input,30,FOLLOW_30_in_expr81087);  
					stream_30.add(char_literal114);

					// AST REWRITE
					// elements: expr1
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 127:18: -> expr1
					{
						adaptor.addChild(root_0, stream_expr1.nextTree());
					}


					retval.tree = root_0;

					}
					break;
				case 3 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:128:4: IDF ( '(' ( exprlist )? ')' -> ^( CALLFUNC IDF ( exprlist )? ) | '[' exprlist ']' -> ^( ARRAYVAL IDF exprlist ) | -> IDF )
					{
					IDF115=(Token)match(input,IDF,FOLLOW_IDF_in_expr81096);  
					stream_IDF.add(IDF115);

					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:129:3: ( '(' ( exprlist )? ')' -> ^( CALLFUNC IDF ( exprlist )? ) | '[' exprlist ']' -> ^( ARRAYVAL IDF exprlist ) | -> IDF )
					int alt30=3;
					switch ( input.LA(1) ) {
					case 29:
						{
						alt30=1;
						}
						break;
					case 45:
						{
						alt30=2;
						}
						break;
					case EOF:
					case 28:
					case 30:
					case 31:
					case 32:
					case 33:
					case 34:
					case 36:
					case 38:
					case 39:
					case 40:
					case 42:
					case 43:
					case 44:
					case 46:
					case 47:
					case 48:
					case 51:
					case 52:
					case 58:
					case 63:
					case 69:
						{
						alt30=3;
						}
						break;
					default:
						NoViableAltException nvae =
							new NoViableAltException("", 30, 0, input);
						throw nvae;
					}
					switch (alt30) {
						case 1 :
							// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:129:5: '(' ( exprlist )? ')'
							{
							char_literal116=(Token)match(input,29,FOLLOW_29_in_expr81103);  
							stream_29.add(char_literal116);

							// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:129:9: ( exprlist )?
							int alt29=2;
							int LA29_0 = input.LA(1);
							if ( (LA29_0==CSTE||LA29_0==IDF||LA29_0==29||LA29_0==34||LA29_0==56) ) {
								alt29=1;
							}
							switch (alt29) {
								case 1 :
									// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:129:9: exprlist
									{
									pushFollow(FOLLOW_exprlist_in_expr81105);
									exprlist117=exprlist();
									state._fsp--;

									stream_exprlist.add(exprlist117.getTree());
									}
									break;

							}

							char_literal118=(Token)match(input,30,FOLLOW_30_in_expr81108);  
							stream_30.add(char_literal118);

							// AST REWRITE
							// elements: exprlist, IDF
							// token labels: 
							// rule labels: retval
							// token list labels: 
							// rule list labels: 
							// wildcard labels: 
							retval.tree = root_0;
							RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

							root_0 = (CommonTree)adaptor.nil();
							// 129:23: -> ^( CALLFUNC IDF ( exprlist )? )
							{
								// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:129:26: ^( CALLFUNC IDF ( exprlist )? )
								{
								CommonTree root_1 = (CommonTree)adaptor.nil();
								root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(CALLFUNC, "CALLFUNC"), root_1);
								adaptor.addChild(root_1, stream_IDF.nextNode());
								// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:129:41: ( exprlist )?
								if ( stream_exprlist.hasNext() ) {
									adaptor.addChild(root_1, stream_exprlist.nextTree());
								}
								stream_exprlist.reset();

								adaptor.addChild(root_0, root_1);
								}

							}


							retval.tree = root_0;

							}
							break;
						case 2 :
							// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:130:5: '[' exprlist ']'
							{
							char_literal119=(Token)match(input,45,FOLLOW_45_in_expr81125);  
							stream_45.add(char_literal119);

							pushFollow(FOLLOW_exprlist_in_expr81127);
							exprlist120=exprlist();
							state._fsp--;

							stream_exprlist.add(exprlist120.getTree());
							char_literal121=(Token)match(input,46,FOLLOW_46_in_expr81129);  
							stream_46.add(char_literal121);

							// AST REWRITE
							// elements: IDF, exprlist
							// token labels: 
							// rule labels: retval
							// token list labels: 
							// rule list labels: 
							// wildcard labels: 
							retval.tree = root_0;
							RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

							root_0 = (CommonTree)adaptor.nil();
							// 130:22: -> ^( ARRAYVAL IDF exprlist )
							{
								// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:130:25: ^( ARRAYVAL IDF exprlist )
								{
								CommonTree root_1 = (CommonTree)adaptor.nil();
								root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(ARRAYVAL, "ARRAYVAL"), root_1);
								adaptor.addChild(root_1, stream_IDF.nextNode());
								adaptor.addChild(root_1, stream_exprlist.nextTree());
								adaptor.addChild(root_0, root_1);
								}

							}


							retval.tree = root_0;

							}
							break;
						case 3 :
							// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:131:5: 
							{
							// AST REWRITE
							// elements: IDF
							// token labels: 
							// rule labels: retval
							// token list labels: 
							// rule list labels: 
							// wildcard labels: 
							retval.tree = root_0;
							RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

							root_0 = (CommonTree)adaptor.nil();
							// 131:5: -> IDF
							{
								adaptor.addChild(root_0, stream_IDF.nextNode());
							}


							retval.tree = root_0;

							}
							break;

					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "expr8"

	// Delegated rules



	public static final BitSet FOLLOW_59_in_program121 = new BitSet(new long[]{0x0000000000020000L});
	public static final BitSet FOLLOW_IDF_in_program123 = new BitSet(new long[]{0x5060000000020000L,0x000000000000001DL});
	public static final BitSet FOLLOW_varsuitdecl_in_program125 = new BitSet(new long[]{0x5060000000020000L,0x000000000000001DL});
	public static final BitSet FOLLOW_funcdecl_in_program128 = new BitSet(new long[]{0x5060000000020000L,0x000000000000001CL});
	public static final BitSet FOLLOW_instr_in_program131 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_64_in_varsuitdecl169 = new BitSet(new long[]{0x0000000000020000L});
	public static final BitSet FOLLOW_identlist_in_varsuitdecl171 = new BitSet(new long[]{0x0000002000000000L});
	public static final BitSet FOLLOW_37_in_varsuitdecl173 = new BitSet(new long[]{0x0086000000000000L,0x0000000000000002L});
	public static final BitSet FOLLOW_typename_in_varsuitdecl175 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_38_in_varsuitdecl177 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDF_in_identlist202 = new BitSet(new long[]{0x0000000200000002L});
	public static final BitSet FOLLOW_33_in_identlist205 = new BitSet(new long[]{0x0000000000020000L});
	public static final BitSet FOLLOW_identlist_in_identlist207 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_atomtype_in_typename228 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_arraytype_in_typename237 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_49_in_arraytype279 = new BitSet(new long[]{0x0000200000000000L});
	public static final BitSet FOLLOW_45_in_arraytype281 = new BitSet(new long[]{0x0000000000001000L});
	public static final BitSet FOLLOW_rangelist_in_arraytype283 = new BitSet(new long[]{0x0000400000000000L});
	public static final BitSet FOLLOW_46_in_arraytype285 = new BitSet(new long[]{0x0200000000000000L});
	public static final BitSet FOLLOW_57_in_arraytype287 = new BitSet(new long[]{0x0084000000000000L,0x0000000000000002L});
	public static final BitSet FOLLOW_atomtype_in_arraytype289 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_CSTE_in_rangelist309 = new BitSet(new long[]{0x0000000800000000L});
	public static final BitSet FOLLOW_35_in_rangelist311 = new BitSet(new long[]{0x0000000000001000L});
	public static final BitSet FOLLOW_CSTE_in_rangelist315 = new BitSet(new long[]{0x0000000200000002L});
	public static final BitSet FOLLOW_33_in_rangelist318 = new BitSet(new long[]{0x0000000000001000L});
	public static final BitSet FOLLOW_rangelist_in_rangelist320 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_53_in_funcdecl346 = new BitSet(new long[]{0x0000000000020000L});
	public static final BitSet FOLLOW_IDF_in_funcdecl348 = new BitSet(new long[]{0x0000000020000000L});
	public static final BitSet FOLLOW_29_in_funcdecl350 = new BitSet(new long[]{0x2000000040020000L});
	public static final BitSet FOLLOW_arglist_in_funcdecl352 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_30_in_funcdecl354 = new BitSet(new long[]{0x0000002000000000L});
	public static final BitSet FOLLOW_37_in_funcdecl356 = new BitSet(new long[]{0x0084000000000000L,0x0000000000000002L});
	public static final BitSet FOLLOW_atomtype_in_funcdecl358 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000011L});
	public static final BitSet FOLLOW_varsuitdecl_in_funcdecl360 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000011L});
	public static final BitSet FOLLOW_68_in_funcdecl363 = new BitSet(new long[]{0x5040000000020000L,0x000000000000001CL});
	public static final BitSet FOLLOW_sequence_in_funcdecl365 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
	public static final BitSet FOLLOW_69_in_funcdecl367 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_arg_in_arglist409 = new BitSet(new long[]{0x0000000200000002L});
	public static final BitSet FOLLOW_33_in_arglist412 = new BitSet(new long[]{0x2000000000020000L});
	public static final BitSet FOLLOW_arglist_in_arglist414 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDF_in_arg438 = new BitSet(new long[]{0x0000002000000000L});
	public static final BitSet FOLLOW_37_in_arg440 = new BitSet(new long[]{0x0086000000000000L,0x0000000000000002L});
	public static final BitSet FOLLOW_typename_in_arg442 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_61_in_arg455 = new BitSet(new long[]{0x0000000000020000L});
	public static final BitSet FOLLOW_IDF_in_arg457 = new BitSet(new long[]{0x0000002000000000L});
	public static final BitSet FOLLOW_37_in_arg459 = new BitSet(new long[]{0x0086000000000000L,0x0000000000000002L});
	public static final BitSet FOLLOW_typename_in_arg461 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_54_in_instr481 = new BitSet(new long[]{0x0100000420021000L});
	public static final BitSet FOLLOW_expr1_in_instr483 = new BitSet(new long[]{0x8000000000000000L});
	public static final BitSet FOLLOW_63_in_instr485 = new BitSet(new long[]{0x5040000000020000L,0x000000000000001CL});
	public static final BitSet FOLLOW_instr_in_instr489 = new BitSet(new long[]{0x0010000000000002L});
	public static final BitSet FOLLOW_52_in_instr499 = new BitSet(new long[]{0x5040000000020000L,0x000000000000001CL});
	public static final BitSet FOLLOW_instr_in_instr503 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_66_in_instr533 = new BitSet(new long[]{0x0100000420021000L});
	public static final BitSet FOLLOW_expr1_in_instr535 = new BitSet(new long[]{0x0008000000000000L});
	public static final BitSet FOLLOW_51_in_instr537 = new BitSet(new long[]{0x5040000000020000L,0x000000000000001CL});
	public static final BitSet FOLLOW_instr_in_instr539 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_62_in_instr558 = new BitSet(new long[]{0x0100000420021002L});
	public static final BitSet FOLLOW_expr1_in_instr560 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDF_in_instr574 = new BitSet(new long[]{0x0000220020000000L});
	public static final BitSet FOLLOW_29_in_instr581 = new BitSet(new long[]{0x0100000460021000L});
	public static final BitSet FOLLOW_exprlist_in_instr583 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_30_in_instr586 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_45_in_instr604 = new BitSet(new long[]{0x0100000420021000L});
	public static final BitSet FOLLOW_exprlist_in_instr606 = new BitSet(new long[]{0x0000400000000000L});
	public static final BitSet FOLLOW_46_in_instr608 = new BitSet(new long[]{0x0000020000000000L});
	public static final BitSet FOLLOW_41_in_instr610 = new BitSet(new long[]{0x0100000420021000L});
	public static final BitSet FOLLOW_expr1_in_instr612 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_41_in_instr635 = new BitSet(new long[]{0x0100000420021000L});
	public static final BitSet FOLLOW_expr1_in_instr637 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_68_in_instr661 = new BitSet(new long[]{0x5040000000020000L,0x000000000000003CL});
	public static final BitSet FOLLOW_sequence_in_instr663 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
	public static final BitSet FOLLOW_69_in_instr666 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_60_in_instr676 = new BitSet(new long[]{0x0000000000020000L});
	public static final BitSet FOLLOW_lvalue_in_instr678 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_67_in_instr691 = new BitSet(new long[]{0x0000000000021000L});
	public static final BitSet FOLLOW_lvalue_in_instr694 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_CSTE_in_instr696 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_instr_in_sequence721 = new BitSet(new long[]{0x0000004000000002L});
	public static final BitSet FOLLOW_38_in_sequence724 = new BitSet(new long[]{0x5040000000020002L,0x000000000000001CL});
	public static final BitSet FOLLOW_sequence_in_sequence726 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expr1_in_exprlist748 = new BitSet(new long[]{0x0000000200000002L});
	public static final BitSet FOLLOW_33_in_exprlist751 = new BitSet(new long[]{0x0100000420021000L});
	public static final BitSet FOLLOW_exprlist_in_exprlist753 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDF_in_lvalue774 = new BitSet(new long[]{0x0000200000000002L});
	public static final BitSet FOLLOW_45_in_lvalue779 = new BitSet(new long[]{0x0100000420021000L});
	public static final BitSet FOLLOW_exprlist_in_lvalue781 = new BitSet(new long[]{0x0000400000000000L});
	public static final BitSet FOLLOW_46_in_lvalue783 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expr2_in_expr1818 = new BitSet(new long[]{0x0400000000000002L});
	public static final BitSet FOLLOW_58_in_expr1824 = new BitSet(new long[]{0x0100000420021000L});
	public static final BitSet FOLLOW_expr1_in_expr1826 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expr3_in_expr2846 = new BitSet(new long[]{0x0001000000000002L});
	public static final BitSet FOLLOW_48_in_expr2852 = new BitSet(new long[]{0x0100000420021000L});
	public static final BitSet FOLLOW_expr2_in_expr2854 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expr4_in_expr3874 = new BitSet(new long[]{0x00001D8010000002L});
	public static final BitSet FOLLOW_operator_in_expr3880 = new BitSet(new long[]{0x0100000420021000L});
	public static final BitSet FOLLOW_expr3_in_expr3882 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expr5_in_expr4928 = new BitSet(new long[]{0x0000000500000002L});
	public static final BitSet FOLLOW_plusMoins_in_expr4934 = new BitSet(new long[]{0x0100000420021000L});
	public static final BitSet FOLLOW_expr4_in_expr4936 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expr6_in_expr5966 = new BitSet(new long[]{0x0000001080000002L});
	public static final BitSet FOLLOW_multDiv_in_expr5972 = new BitSet(new long[]{0x0100000420021000L});
	public static final BitSet FOLLOW_expr5_in_expr5974 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expr7_in_expr61005 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_opun_in_expr61013 = new BitSet(new long[]{0x0000000020021000L});
	public static final BitSet FOLLOW_expr7_in_expr61015 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expr8_in_expr71042 = new BitSet(new long[]{0x0000800000000002L});
	public static final BitSet FOLLOW_47_in_expr71049 = new BitSet(new long[]{0x0000000020021000L});
	public static final BitSet FOLLOW_expr7_in_expr71051 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_CSTE_in_expr81074 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_29_in_expr81083 = new BitSet(new long[]{0x0100000420021000L});
	public static final BitSet FOLLOW_expr1_in_expr81085 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_30_in_expr81087 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDF_in_expr81096 = new BitSet(new long[]{0x0000200020000002L});
	public static final BitSet FOLLOW_29_in_expr81103 = new BitSet(new long[]{0x0100000460021000L});
	public static final BitSet FOLLOW_exprlist_in_expr81105 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_30_in_expr81108 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_45_in_expr81125 = new BitSet(new long[]{0x0100000420021000L});
	public static final BitSet FOLLOW_exprlist_in_expr81127 = new BitSet(new long[]{0x0000400000000000L});
	public static final BitSet FOLLOW_46_in_expr81129 = new BitSet(new long[]{0x0000000000000002L});
}

grammar Projet;

options {
	k=1;
	output = AST;
	ASTLabelType=CommonTree;
}

tokens{
	VARDECLIST;
	FUNCS;
	BODY;
	VARDEC;
	FUNCDEC;
	ARGLIST;
	IF;
	ELSE;
	WHILE;
	COND;
	AFFECT;
	RETURN;
	READ;
	WRITE;
	EXPR;
	CALLFUNC;
	ARRAY;
	ARRAYVAL;
	REFFALSE;
	REFTRUE;
}

program
	: 'program' IDF varsuitdecl* funcdecl* instr -> ^(VARDECLIST varsuitdecl*) ^(FUNCS funcdecl*) ^(BODY instr) EOF
	;
   
varsuitdecl
	: 'var' identlist ':' typename ';' -> ^(VARDEC typename identlist)
    ;

identlist 
	: IDF (',' identlist)? -> IDF identlist?
	;

typename 
	: atomtype -> atomtype
	| arraytype -> ^(ARRAY arraytype)
	;

atomtype 
	: 'void'
	| 'bool'
	| 'int'
	;

arraytype 
	: 'array' '[' rangelist ']' 'of' atomtype -> atomtype rangelist
	;

rangelist 
	: start=CSTE '..' stop=CSTE (',' rangelist)? -> $start $stop rangelist?
	;
	
funcdecl 
	: 'function' IDF '(' arglist ')' ':' atomtype varsuitdecl* '{' sequence '}' -> ^(FUNCDEC atomtype IDF ^(ARGLIST arglist?) ^(VARDECLIST varsuitdecl*) ^(BODY sequence))
	;

arglist 
	: arg (',' arglist)? -> arg arglist?
	|
	;

arg 
	: IDF ':' typename -> IDF typename REFFALSE
	| 'ref' IDF ':' typename -> IDF typename REFTRUE
	;

instr 
	: 'if' expr1 'then' then=instr (options{greedy=true;}:'else' els=instr)? -> ^(IF expr1 ^(BODY $then) ^(ELSE $els)?)
	| 'while' expr1 'do' instr -> ^(WHILE expr1 ^(BODY instr))
	| 'return' expr1? -> ^(RETURN expr1)
	| IDF 
		( '(' exprlist? ')' -> ^(CALLFUNC IDF exprlist?)
		|	('[' exprlist ']' '=' expr1 -> ^(AFFECT ^(ARRAYVAL IDF exprlist) expr1)
			| '=' expr1 -> ^(AFFECT IDF expr1)
			)
		)
	| '{' sequence? '}' -> sequence?
	| 'read' lvalue -> ^(READ lvalue)
	| 'write' (lvalue|CSTE) -> ^(WRITE lvalue? CSTE?)
	;

sequence 
	: instr (';' sequence?)? -> instr sequence?
	;

exprlist 
	: expr1 (',' exprlist)? -> expr1 exprlist?
	;

lvalue 
	: IDF
		('[' exprlist ']'-> ^(ARRAYVAL IDF exprlist)
		| -> IDF
		)
	;
				
expr1 : expr2 (->expr2|'or' expr1 -> ^('or' expr2 expr1));
	
expr2 : expr3 (->expr3|'and' expr2 -> ^('and' expr3 expr2));
	
expr3 : expr4 (->expr4|operator expr3 -> ^(operator expr4 expr3));
operator : '==' | '<=' | '>=' | '<' | '>' | '!=';

expr4 : expr5 (->expr5|plusMoins expr4 -> ^(plusMoins expr5 expr4));
plusMoins : '+' | '-';

expr5 : expr6 (->expr6|multDiv expr5 -> ^(multDiv expr6 expr5));
multDiv : '*' | '/';
	
expr6 : expr7 -> expr7 | opun expr7 -> ^(opun expr7);
opun : '-' | 'not';

expr7 : expr8 (->expr8|('^' expr7) -> ^('^' expr8 expr7));
	
expr8 
	: CSTE -> CSTE
	| '(' expr1 ')' -> expr1
	| IDF 
		( '(' exprlist? ')' -> ^(CALLFUNC IDF exprlist?)
		| '[' exprlist ']' -> ^(ARRAYVAL IDF exprlist)
		| -> IDF
		)
	; 

// constantes
//LETTRE : 'a'..'z' | 'A'..'Z';
//CHIFFRE : '0'..'9' ;
IDF  : ('a'..'z' | 'A'..'Z') ('a'..'z' | 'A'..'Z' | '0'..'9')* ;
CSTE : '-'? ('0'..'9')+ | '"' .* '"' | 'true' | 'false';
WHITESPACE : (' ' | '\t' | '\r'? '\n') {$channel=HIDDEN;};
COMMENTAIRE : '/*' .* '*/' {$channel=HIDDEN;};
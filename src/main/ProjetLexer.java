// $ANTLR null /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g 2019-03-17 11:24:39
package main;

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class ProjetLexer extends Lexer {
	public static final int EOF=-1;
	public static final int T__28=28;
	public static final int T__29=29;
	public static final int T__30=30;
	public static final int T__31=31;
	public static final int T__32=32;
	public static final int T__33=33;
	public static final int T__34=34;
	public static final int T__35=35;
	public static final int T__36=36;
	public static final int T__37=37;
	public static final int T__38=38;
	public static final int T__39=39;
	public static final int T__40=40;
	public static final int T__41=41;
	public static final int T__42=42;
	public static final int T__43=43;
	public static final int T__44=44;
	public static final int T__45=45;
	public static final int T__46=46;
	public static final int T__47=47;
	public static final int T__48=48;
	public static final int T__49=49;
	public static final int T__50=50;
	public static final int T__51=51;
	public static final int T__52=52;
	public static final int T__53=53;
	public static final int T__54=54;
	public static final int T__55=55;
	public static final int T__56=56;
	public static final int T__57=57;
	public static final int T__58=58;
	public static final int T__59=59;
	public static final int T__60=60;
	public static final int T__61=61;
	public static final int T__62=62;
	public static final int T__63=63;
	public static final int T__64=64;
	public static final int T__65=65;
	public static final int T__66=66;
	public static final int T__67=67;
	public static final int T__68=68;
	public static final int T__69=69;
	public static final int AFFECT=4;
	public static final int ARGLIST=5;
	public static final int ARRAY=6;
	public static final int ARRAYVAL=7;
	public static final int BODY=8;
	public static final int CALLFUNC=9;
	public static final int COMMENTAIRE=10;
	public static final int COND=11;
	public static final int CSTE=12;
	public static final int ELSE=13;
	public static final int EXPR=14;
	public static final int FUNCDEC=15;
	public static final int FUNCS=16;
	public static final int IDF=17;
	public static final int IF=18;
	public static final int READ=19;
	public static final int REFFALSE=20;
	public static final int REFTRUE=21;
	public static final int RETURN=22;
	public static final int VARDEC=23;
	public static final int VARDECLIST=24;
	public static final int WHILE=25;
	public static final int WHITESPACE=26;
	public static final int WRITE=27;

	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public ProjetLexer() {} 
	public ProjetLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public ProjetLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "/home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g"; }

	// $ANTLR start "T__28"
	public final void mT__28() throws RecognitionException {
		try {
			int _type = T__28;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:2:7: ( '!=' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:2:9: '!='
			{
			match("!="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__28"

	// $ANTLR start "T__29"
	public final void mT__29() throws RecognitionException {
		try {
			int _type = T__29;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:3:7: ( '(' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:3:9: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__29"

	// $ANTLR start "T__30"
	public final void mT__30() throws RecognitionException {
		try {
			int _type = T__30;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:4:7: ( ')' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:4:9: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__30"

	// $ANTLR start "T__31"
	public final void mT__31() throws RecognitionException {
		try {
			int _type = T__31;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:5:7: ( '*' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:5:9: '*'
			{
			match('*'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__31"

	// $ANTLR start "T__32"
	public final void mT__32() throws RecognitionException {
		try {
			int _type = T__32;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:6:7: ( '+' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:6:9: '+'
			{
			match('+'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__32"

	// $ANTLR start "T__33"
	public final void mT__33() throws RecognitionException {
		try {
			int _type = T__33;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:7:7: ( ',' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:7:9: ','
			{
			match(','); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__33"

	// $ANTLR start "T__34"
	public final void mT__34() throws RecognitionException {
		try {
			int _type = T__34;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:8:7: ( '-' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:8:9: '-'
			{
			match('-'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__34"

	// $ANTLR start "T__35"
	public final void mT__35() throws RecognitionException {
		try {
			int _type = T__35;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:9:7: ( '..' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:9:9: '..'
			{
			match(".."); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__35"

	// $ANTLR start "T__36"
	public final void mT__36() throws RecognitionException {
		try {
			int _type = T__36;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:10:7: ( '/' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:10:9: '/'
			{
			match('/'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__36"

	// $ANTLR start "T__37"
	public final void mT__37() throws RecognitionException {
		try {
			int _type = T__37;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:11:7: ( ':' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:11:9: ':'
			{
			match(':'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__37"

	// $ANTLR start "T__38"
	public final void mT__38() throws RecognitionException {
		try {
			int _type = T__38;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:12:7: ( ';' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:12:9: ';'
			{
			match(';'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__38"

	// $ANTLR start "T__39"
	public final void mT__39() throws RecognitionException {
		try {
			int _type = T__39;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:13:7: ( '<' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:13:9: '<'
			{
			match('<'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__39"

	// $ANTLR start "T__40"
	public final void mT__40() throws RecognitionException {
		try {
			int _type = T__40;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:14:7: ( '<=' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:14:9: '<='
			{
			match("<="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__40"

	// $ANTLR start "T__41"
	public final void mT__41() throws RecognitionException {
		try {
			int _type = T__41;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:15:7: ( '=' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:15:9: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__41"

	// $ANTLR start "T__42"
	public final void mT__42() throws RecognitionException {
		try {
			int _type = T__42;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:16:7: ( '==' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:16:9: '=='
			{
			match("=="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__42"

	// $ANTLR start "T__43"
	public final void mT__43() throws RecognitionException {
		try {
			int _type = T__43;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:17:7: ( '>' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:17:9: '>'
			{
			match('>'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__43"

	// $ANTLR start "T__44"
	public final void mT__44() throws RecognitionException {
		try {
			int _type = T__44;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:18:7: ( '>=' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:18:9: '>='
			{
			match(">="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__44"

	// $ANTLR start "T__45"
	public final void mT__45() throws RecognitionException {
		try {
			int _type = T__45;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:19:7: ( '[' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:19:9: '['
			{
			match('['); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__45"

	// $ANTLR start "T__46"
	public final void mT__46() throws RecognitionException {
		try {
			int _type = T__46;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:20:7: ( ']' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:20:9: ']'
			{
			match(']'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__46"

	// $ANTLR start "T__47"
	public final void mT__47() throws RecognitionException {
		try {
			int _type = T__47;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:21:7: ( '^' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:21:9: '^'
			{
			match('^'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__47"

	// $ANTLR start "T__48"
	public final void mT__48() throws RecognitionException {
		try {
			int _type = T__48;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:22:7: ( 'and' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:22:9: 'and'
			{
			match("and"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__48"

	// $ANTLR start "T__49"
	public final void mT__49() throws RecognitionException {
		try {
			int _type = T__49;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:23:7: ( 'array' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:23:9: 'array'
			{
			match("array"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__49"

	// $ANTLR start "T__50"
	public final void mT__50() throws RecognitionException {
		try {
			int _type = T__50;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:24:7: ( 'bool' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:24:9: 'bool'
			{
			match("bool"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__50"

	// $ANTLR start "T__51"
	public final void mT__51() throws RecognitionException {
		try {
			int _type = T__51;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:25:7: ( 'do' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:25:9: 'do'
			{
			match("do"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__51"

	// $ANTLR start "T__52"
	public final void mT__52() throws RecognitionException {
		try {
			int _type = T__52;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:26:7: ( 'else' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:26:9: 'else'
			{
			match("else"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__52"

	// $ANTLR start "T__53"
	public final void mT__53() throws RecognitionException {
		try {
			int _type = T__53;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:27:7: ( 'function' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:27:9: 'function'
			{
			match("function"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__53"

	// $ANTLR start "T__54"
	public final void mT__54() throws RecognitionException {
		try {
			int _type = T__54;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:28:7: ( 'if' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:28:9: 'if'
			{
			match("if"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__54"

	// $ANTLR start "T__55"
	public final void mT__55() throws RecognitionException {
		try {
			int _type = T__55;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:29:7: ( 'int' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:29:9: 'int'
			{
			match("int"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__55"

	// $ANTLR start "T__56"
	public final void mT__56() throws RecognitionException {
		try {
			int _type = T__56;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:30:7: ( 'not' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:30:9: 'not'
			{
			match("not"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__56"

	// $ANTLR start "T__57"
	public final void mT__57() throws RecognitionException {
		try {
			int _type = T__57;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:31:7: ( 'of' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:31:9: 'of'
			{
			match("of"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__57"

	// $ANTLR start "T__58"
	public final void mT__58() throws RecognitionException {
		try {
			int _type = T__58;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:32:7: ( 'or' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:32:9: 'or'
			{
			match("or"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__58"

	// $ANTLR start "T__59"
	public final void mT__59() throws RecognitionException {
		try {
			int _type = T__59;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:33:7: ( 'program' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:33:9: 'program'
			{
			match("program"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__59"

	// $ANTLR start "T__60"
	public final void mT__60() throws RecognitionException {
		try {
			int _type = T__60;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:34:7: ( 'read' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:34:9: 'read'
			{
			match("read"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__60"

	// $ANTLR start "T__61"
	public final void mT__61() throws RecognitionException {
		try {
			int _type = T__61;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:35:7: ( 'ref' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:35:9: 'ref'
			{
			match("ref"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__61"

	// $ANTLR start "T__62"
	public final void mT__62() throws RecognitionException {
		try {
			int _type = T__62;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:36:7: ( 'return' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:36:9: 'return'
			{
			match("return"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__62"

	// $ANTLR start "T__63"
	public final void mT__63() throws RecognitionException {
		try {
			int _type = T__63;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:37:7: ( 'then' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:37:9: 'then'
			{
			match("then"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__63"

	// $ANTLR start "T__64"
	public final void mT__64() throws RecognitionException {
		try {
			int _type = T__64;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:38:7: ( 'var' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:38:9: 'var'
			{
			match("var"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__64"

	// $ANTLR start "T__65"
	public final void mT__65() throws RecognitionException {
		try {
			int _type = T__65;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:39:7: ( 'void' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:39:9: 'void'
			{
			match("void"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__65"

	// $ANTLR start "T__66"
	public final void mT__66() throws RecognitionException {
		try {
			int _type = T__66;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:40:7: ( 'while' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:40:9: 'while'
			{
			match("while"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__66"

	// $ANTLR start "T__67"
	public final void mT__67() throws RecognitionException {
		try {
			int _type = T__67;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:41:7: ( 'write' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:41:9: 'write'
			{
			match("write"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__67"

	// $ANTLR start "T__68"
	public final void mT__68() throws RecognitionException {
		try {
			int _type = T__68;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:42:7: ( '{' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:42:9: '{'
			{
			match('{'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__68"

	// $ANTLR start "T__69"
	public final void mT__69() throws RecognitionException {
		try {
			int _type = T__69;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:43:7: ( '}' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:43:9: '}'
			{
			match('}'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__69"

	// $ANTLR start "IDF"
	public final void mIDF() throws RecognitionException {
		try {
			int _type = IDF;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:138:6: ( ( 'a' .. 'z' | 'A' .. 'Z' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )* )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:138:8: ( 'a' .. 'z' | 'A' .. 'Z' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )*
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:138:30: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )*
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( ((LA1_0 >= '0' && LA1_0 <= '9')||(LA1_0 >= 'A' && LA1_0 <= 'Z')||(LA1_0 >= 'a' && LA1_0 <= 'z')) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop1;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IDF"

	// $ANTLR start "CSTE"
	public final void mCSTE() throws RecognitionException {
		try {
			int _type = CSTE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:139:6: ( ( '-' )? ( '0' .. '9' )+ | '\"' ( . )* '\"' | 'true' | 'false' )
			int alt5=4;
			switch ( input.LA(1) ) {
			case '-':
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				{
				alt5=1;
				}
				break;
			case '\"':
				{
				alt5=2;
				}
				break;
			case 't':
				{
				alt5=3;
				}
				break;
			case 'f':
				{
				alt5=4;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 5, 0, input);
				throw nvae;
			}
			switch (alt5) {
				case 1 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:139:8: ( '-' )? ( '0' .. '9' )+
					{
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:139:8: ( '-' )?
					int alt2=2;
					int LA2_0 = input.LA(1);
					if ( (LA2_0=='-') ) {
						alt2=1;
					}
					switch (alt2) {
						case 1 :
							// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:139:8: '-'
							{
							match('-'); 
							}
							break;

					}

					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:139:13: ( '0' .. '9' )+
					int cnt3=0;
					loop3:
					while (true) {
						int alt3=2;
						int LA3_0 = input.LA(1);
						if ( ((LA3_0 >= '0' && LA3_0 <= '9')) ) {
							alt3=1;
						}

						switch (alt3) {
						case 1 :
							// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt3 >= 1 ) break loop3;
							EarlyExitException eee = new EarlyExitException(3, input);
							throw eee;
						}
						cnt3++;
					}

					}
					break;
				case 2 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:139:26: '\"' ( . )* '\"'
					{
					match('\"'); 
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:139:30: ( . )*
					loop4:
					while (true) {
						int alt4=2;
						int LA4_0 = input.LA(1);
						if ( (LA4_0=='\"') ) {
							alt4=2;
						}
						else if ( ((LA4_0 >= '\u0000' && LA4_0 <= '!')||(LA4_0 >= '#' && LA4_0 <= '\uFFFF')) ) {
							alt4=1;
						}

						switch (alt4) {
						case 1 :
							// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:139:30: .
							{
							matchAny(); 
							}
							break;

						default :
							break loop4;
						}
					}

					match('\"'); 
					}
					break;
				case 3 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:139:39: 'true'
					{
					match("true"); 

					}
					break;
				case 4 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:139:48: 'false'
					{
					match("false"); 

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CSTE"

	// $ANTLR start "WHITESPACE"
	public final void mWHITESPACE() throws RecognitionException {
		try {
			int _type = WHITESPACE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:140:12: ( ( ' ' | '\\t' | ( '\\r' )? '\\n' ) )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:140:14: ( ' ' | '\\t' | ( '\\r' )? '\\n' )
			{
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:140:14: ( ' ' | '\\t' | ( '\\r' )? '\\n' )
			int alt7=3;
			switch ( input.LA(1) ) {
			case ' ':
				{
				alt7=1;
				}
				break;
			case '\t':
				{
				alt7=2;
				}
				break;
			case '\n':
			case '\r':
				{
				alt7=3;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 7, 0, input);
				throw nvae;
			}
			switch (alt7) {
				case 1 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:140:15: ' '
					{
					match(' '); 
					}
					break;
				case 2 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:140:21: '\\t'
					{
					match('\t'); 
					}
					break;
				case 3 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:140:28: ( '\\r' )? '\\n'
					{
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:140:28: ( '\\r' )?
					int alt6=2;
					int LA6_0 = input.LA(1);
					if ( (LA6_0=='\r') ) {
						alt6=1;
					}
					switch (alt6) {
						case 1 :
							// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:140:28: '\\r'
							{
							match('\r'); 
							}
							break;

					}

					match('\n'); 
					}
					break;

			}

			_channel=HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WHITESPACE"

	// $ANTLR start "COMMENTAIRE"
	public final void mCOMMENTAIRE() throws RecognitionException {
		try {
			int _type = COMMENTAIRE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:141:13: ( '/*' ( . )* '*/' )
			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:141:15: '/*' ( . )* '*/'
			{
			match("/*"); 

			// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:141:20: ( . )*
			loop8:
			while (true) {
				int alt8=2;
				int LA8_0 = input.LA(1);
				if ( (LA8_0=='*') ) {
					int LA8_1 = input.LA(2);
					if ( (LA8_1=='/') ) {
						alt8=2;
					}
					else if ( ((LA8_1 >= '\u0000' && LA8_1 <= '.')||(LA8_1 >= '0' && LA8_1 <= '\uFFFF')) ) {
						alt8=1;
					}

				}
				else if ( ((LA8_0 >= '\u0000' && LA8_0 <= ')')||(LA8_0 >= '+' && LA8_0 <= '\uFFFF')) ) {
					alt8=1;
				}

				switch (alt8) {
				case 1 :
					// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:141:20: .
					{
					matchAny(); 
					}
					break;

				default :
					break loop8;
				}
			}

			match("*/"); 

			_channel=HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMENTAIRE"

	@Override
	public void mTokens() throws RecognitionException {
		// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:8: ( T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | T__60 | T__61 | T__62 | T__63 | T__64 | T__65 | T__66 | T__67 | T__68 | T__69 | IDF | CSTE | WHITESPACE | COMMENTAIRE )
		int alt9=46;
		alt9 = dfa9.predict(input);
		switch (alt9) {
			case 1 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:10: T__28
				{
				mT__28(); 

				}
				break;
			case 2 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:16: T__29
				{
				mT__29(); 

				}
				break;
			case 3 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:22: T__30
				{
				mT__30(); 

				}
				break;
			case 4 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:28: T__31
				{
				mT__31(); 

				}
				break;
			case 5 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:34: T__32
				{
				mT__32(); 

				}
				break;
			case 6 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:40: T__33
				{
				mT__33(); 

				}
				break;
			case 7 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:46: T__34
				{
				mT__34(); 

				}
				break;
			case 8 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:52: T__35
				{
				mT__35(); 

				}
				break;
			case 9 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:58: T__36
				{
				mT__36(); 

				}
				break;
			case 10 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:64: T__37
				{
				mT__37(); 

				}
				break;
			case 11 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:70: T__38
				{
				mT__38(); 

				}
				break;
			case 12 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:76: T__39
				{
				mT__39(); 

				}
				break;
			case 13 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:82: T__40
				{
				mT__40(); 

				}
				break;
			case 14 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:88: T__41
				{
				mT__41(); 

				}
				break;
			case 15 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:94: T__42
				{
				mT__42(); 

				}
				break;
			case 16 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:100: T__43
				{
				mT__43(); 

				}
				break;
			case 17 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:106: T__44
				{
				mT__44(); 

				}
				break;
			case 18 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:112: T__45
				{
				mT__45(); 

				}
				break;
			case 19 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:118: T__46
				{
				mT__46(); 

				}
				break;
			case 20 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:124: T__47
				{
				mT__47(); 

				}
				break;
			case 21 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:130: T__48
				{
				mT__48(); 

				}
				break;
			case 22 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:136: T__49
				{
				mT__49(); 

				}
				break;
			case 23 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:142: T__50
				{
				mT__50(); 

				}
				break;
			case 24 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:148: T__51
				{
				mT__51(); 

				}
				break;
			case 25 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:154: T__52
				{
				mT__52(); 

				}
				break;
			case 26 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:160: T__53
				{
				mT__53(); 

				}
				break;
			case 27 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:166: T__54
				{
				mT__54(); 

				}
				break;
			case 28 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:172: T__55
				{
				mT__55(); 

				}
				break;
			case 29 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:178: T__56
				{
				mT__56(); 

				}
				break;
			case 30 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:184: T__57
				{
				mT__57(); 

				}
				break;
			case 31 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:190: T__58
				{
				mT__58(); 

				}
				break;
			case 32 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:196: T__59
				{
				mT__59(); 

				}
				break;
			case 33 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:202: T__60
				{
				mT__60(); 

				}
				break;
			case 34 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:208: T__61
				{
				mT__61(); 

				}
				break;
			case 35 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:214: T__62
				{
				mT__62(); 

				}
				break;
			case 36 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:220: T__63
				{
				mT__63(); 

				}
				break;
			case 37 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:226: T__64
				{
				mT__64(); 

				}
				break;
			case 38 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:232: T__65
				{
				mT__65(); 

				}
				break;
			case 39 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:238: T__66
				{
				mT__66(); 

				}
				break;
			case 40 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:244: T__67
				{
				mT__67(); 

				}
				break;
			case 41 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:250: T__68
				{
				mT__68(); 

				}
				break;
			case 42 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:256: T__69
				{
				mT__69(); 

				}
				break;
			case 43 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:262: IDF
				{
				mIDF(); 

				}
				break;
			case 44 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:266: CSTE
				{
				mCSTE(); 

				}
				break;
			case 45 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:271: WHITESPACE
				{
				mWHITESPACE(); 

				}
				break;
			case 46 :
				// /home/ophelien/TNCY/Traduction/Telecom_Traduction_2019/src/main/Projet.g:1:282: COMMENTAIRE
				{
				mCOMMENTAIRE(); 

				}
				break;

		}
	}


	protected DFA9 dfa9 = new DFA9(this);
	static final String DFA9_eotS =
		"\7\uffff\1\44\1\uffff\1\46\2\uffff\1\50\1\52\1\54\3\uffff\15\41\16\uffff"+
		"\3\41\1\104\3\41\1\110\2\41\1\113\1\114\10\41\1\127\2\41\1\uffff\3\41"+
		"\1\uffff\1\135\1\136\2\uffff\2\41\1\141\3\41\1\145\3\41\1\uffff\1\41\1"+
		"\152\1\153\2\41\2\uffff\1\41\1\157\1\uffff\1\41\1\161\2\uffff\1\162\2"+
		"\41\1\165\2\uffff\1\41\1\uffff\1\41\1\uffff\1\41\2\uffff\1\171\1\172\1"+
		"\uffff\2\41\1\175\2\uffff\1\41\1\177\1\uffff\1\u0080\2\uffff";
	static final String DFA9_eofS =
		"\u0081\uffff";
	static final String DFA9_minS =
		"\1\11\6\uffff\1\60\1\uffff\1\52\2\uffff\3\75\3\uffff\1\156\2\157\1\154"+
		"\1\141\1\146\1\157\1\146\1\162\1\145\1\150\1\141\1\150\16\uffff\1\144"+
		"\1\162\1\157\1\60\1\163\1\156\1\154\1\60\2\164\2\60\1\157\1\141\1\145"+
		"\1\165\1\162\3\151\1\60\1\141\1\154\1\uffff\1\145\1\143\1\163\1\uffff"+
		"\2\60\2\uffff\1\147\1\144\1\60\1\165\1\156\1\145\1\60\1\144\1\154\1\164"+
		"\1\uffff\1\171\2\60\1\164\1\145\2\uffff\1\162\1\60\1\uffff\1\162\1\60"+
		"\2\uffff\1\60\2\145\1\60\2\uffff\1\151\1\uffff\1\141\1\uffff\1\156\2\uffff"+
		"\2\60\1\uffff\1\157\1\155\1\60\2\uffff\1\156\1\60\1\uffff\1\60\2\uffff";
	static final String DFA9_maxS =
		"\1\175\6\uffff\1\71\1\uffff\1\52\2\uffff\3\75\3\uffff\1\162\2\157\1\154"+
		"\1\165\1\156\1\157\2\162\1\145\1\162\1\157\1\162\16\uffff\1\144\1\162"+
		"\1\157\1\172\1\163\1\156\1\154\1\172\2\164\2\172\1\157\1\164\1\145\1\165"+
		"\1\162\3\151\1\172\1\141\1\154\1\uffff\1\145\1\143\1\163\1\uffff\2\172"+
		"\2\uffff\1\147\1\144\1\172\1\165\1\156\1\145\1\172\1\144\1\154\1\164\1"+
		"\uffff\1\171\2\172\1\164\1\145\2\uffff\1\162\1\172\1\uffff\1\162\1\172"+
		"\2\uffff\1\172\2\145\1\172\2\uffff\1\151\1\uffff\1\141\1\uffff\1\156\2"+
		"\uffff\2\172\1\uffff\1\157\1\155\1\172\2\uffff\1\156\1\172\1\uffff\1\172"+
		"\2\uffff";
	static final String DFA9_acceptS =
		"\1\uffff\1\1\1\2\1\3\1\4\1\5\1\6\1\uffff\1\10\1\uffff\1\12\1\13\3\uffff"+
		"\1\22\1\23\1\24\15\uffff\1\51\1\52\1\53\1\54\1\55\1\7\1\56\1\11\1\15\1"+
		"\14\1\17\1\16\1\21\1\20\27\uffff\1\30\3\uffff\1\33\2\uffff\1\36\1\37\12"+
		"\uffff\1\25\5\uffff\1\34\1\35\2\uffff\1\42\2\uffff\1\53\1\45\4\uffff\1"+
		"\27\1\31\1\uffff\1\53\1\uffff\1\41\1\uffff\1\44\1\46\2\uffff\1\26\3\uffff"+
		"\1\47\1\50\2\uffff\1\43\1\uffff\1\40\1\32";
	static final String DFA9_specialS =
		"\u0081\uffff}>";
	static final String[] DFA9_transitionS = {
			"\2\43\2\uffff\1\43\22\uffff\1\43\1\1\1\42\5\uffff\1\2\1\3\1\4\1\5\1\6"+
			"\1\7\1\10\1\11\12\42\1\12\1\13\1\14\1\15\1\16\2\uffff\32\41\1\17\1\uffff"+
			"\1\20\1\21\2\uffff\1\22\1\23\1\41\1\24\1\25\1\26\2\41\1\27\4\41\1\30"+
			"\1\31\1\32\1\41\1\33\1\41\1\34\1\41\1\35\1\36\3\41\1\37\1\uffff\1\40",
			"",
			"",
			"",
			"",
			"",
			"",
			"\12\42",
			"",
			"\1\45",
			"",
			"",
			"\1\47",
			"\1\51",
			"\1\53",
			"",
			"",
			"",
			"\1\55\3\uffff\1\56",
			"\1\57",
			"\1\60",
			"\1\61",
			"\1\63\23\uffff\1\62",
			"\1\64\7\uffff\1\65",
			"\1\66",
			"\1\67\13\uffff\1\70",
			"\1\71",
			"\1\72",
			"\1\73\11\uffff\1\74",
			"\1\75\15\uffff\1\76",
			"\1\77\11\uffff\1\100",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\101",
			"\1\102",
			"\1\103",
			"\12\41\7\uffff\32\41\6\uffff\32\41",
			"\1\105",
			"\1\106",
			"\1\107",
			"\12\41\7\uffff\32\41\6\uffff\32\41",
			"\1\111",
			"\1\112",
			"\12\41\7\uffff\32\41\6\uffff\32\41",
			"\12\41\7\uffff\32\41\6\uffff\32\41",
			"\1\115",
			"\1\116\4\uffff\1\117\15\uffff\1\120",
			"\1\121",
			"\1\122",
			"\1\123",
			"\1\124",
			"\1\125",
			"\1\126",
			"\12\41\7\uffff\32\41\6\uffff\32\41",
			"\1\130",
			"\1\131",
			"",
			"\1\132",
			"\1\133",
			"\1\134",
			"",
			"\12\41\7\uffff\32\41\6\uffff\32\41",
			"\12\41\7\uffff\32\41\6\uffff\32\41",
			"",
			"",
			"\1\137",
			"\1\140",
			"\12\41\7\uffff\32\41\6\uffff\32\41",
			"\1\142",
			"\1\143",
			"\1\144",
			"\12\41\7\uffff\32\41\6\uffff\32\41",
			"\1\146",
			"\1\147",
			"\1\150",
			"",
			"\1\151",
			"\12\41\7\uffff\32\41\6\uffff\32\41",
			"\12\41\7\uffff\32\41\6\uffff\32\41",
			"\1\154",
			"\1\155",
			"",
			"",
			"\1\156",
			"\12\41\7\uffff\32\41\6\uffff\32\41",
			"",
			"\1\160",
			"\12\41\7\uffff\32\41\6\uffff\32\41",
			"",
			"",
			"\12\41\7\uffff\32\41\6\uffff\32\41",
			"\1\163",
			"\1\164",
			"\12\41\7\uffff\32\41\6\uffff\32\41",
			"",
			"",
			"\1\166",
			"",
			"\1\167",
			"",
			"\1\170",
			"",
			"",
			"\12\41\7\uffff\32\41\6\uffff\32\41",
			"\12\41\7\uffff\32\41\6\uffff\32\41",
			"",
			"\1\173",
			"\1\174",
			"\12\41\7\uffff\32\41\6\uffff\32\41",
			"",
			"",
			"\1\176",
			"\12\41\7\uffff\32\41\6\uffff\32\41",
			"",
			"\12\41\7\uffff\32\41\6\uffff\32\41",
			"",
			""
	};

	static final short[] DFA9_eot = DFA.unpackEncodedString(DFA9_eotS);
	static final short[] DFA9_eof = DFA.unpackEncodedString(DFA9_eofS);
	static final char[] DFA9_min = DFA.unpackEncodedStringToUnsignedChars(DFA9_minS);
	static final char[] DFA9_max = DFA.unpackEncodedStringToUnsignedChars(DFA9_maxS);
	static final short[] DFA9_accept = DFA.unpackEncodedString(DFA9_acceptS);
	static final short[] DFA9_special = DFA.unpackEncodedString(DFA9_specialS);
	static final short[][] DFA9_transition;

	static {
		int numStates = DFA9_transitionS.length;
		DFA9_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA9_transition[i] = DFA.unpackEncodedString(DFA9_transitionS[i]);
		}
	}

	protected class DFA9 extends DFA {

		public DFA9(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 9;
			this.eot = DFA9_eot;
			this.eof = DFA9_eof;
			this.min = DFA9_min;
			this.max = DFA9_max;
			this.accept = DFA9_accept;
			this.special = DFA9_special;
			this.transition = DFA9_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | T__60 | T__61 | T__62 | T__63 | T__64 | T__65 | T__66 | T__67 | T__68 | T__69 | IDF | CSTE | WHITESPACE | COMMENTAIRE );";
		}
	}

}

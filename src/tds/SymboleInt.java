package tds;

public class SymboleInt extends Symbole {

    public static int size = 8;

    public SymboleInt(String i, int o){
        super(i, o, "int");
    }
}

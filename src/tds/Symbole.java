package tds;

public abstract class Symbole {

    private String idf;
    private int offset;
    private boolean initialized;
    private String type;

    /**
     * Constructor
     * @param i for identifier
     * @param o for offset into the memory stack
     * @param t for type of the symbol
     */
    public Symbole(String i, int o, String t){
        this.idf = i;
        this.offset = o;
        this.initialized = false;
        this.type = t;
    }

    /**
     * get the identifier of this symbol
     * @return identifier
     */
    public String getIdentifier() {
        return this.idf;
    }

    /**
     * Get the offset of this symbol into the memory stack
     * @return offset
     */
    public int getOffset() {
        return this.offset;
    }

    /**
     * Determine if the symbol has been initialized
     * @return true if the symbol has been initialized before, else it would return false
     */
    public boolean isInitialized(){
        return this.initialized;
    }

    /**
     * Initialize the symbol
     */
    public void initialize(){
        this.initialized = true;
    }

    /**
     * Get the symbole type
     * @return symbole type
     */
    public String getType(){
        return this.type;
    }

}

package tds;

import java.util.ArrayList;

public class SymboleArray extends Symbole {

    private ArrayList<int[]> shape;

    public SymboleArray(String i, int o, String t, ArrayList<int[]> s){
        super(i, o, "array[" + t + "][" + s.size() + "]");
        this.shape = s;
    }

    /**
     * Get the shape of the array, for instance {[-3,3];[0,5]} stands for a 2D-Array with the first line going from
     * -3 to 3 and the second one from 0 to 5
     * @return shape
     */
    public ArrayList<int[]> getShape(){
        return this.shape;
    }

}

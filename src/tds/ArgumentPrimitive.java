package tds;

import java.util.ArrayList;

public class ArgumentPrimitive extends Argument {

    public ArgumentPrimitive(String idf, String type, boolean ref){
        super(idf, "primitive", type, new ArrayList<>(), ref);
    }

}

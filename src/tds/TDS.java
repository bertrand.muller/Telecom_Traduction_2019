package tds;

import java.util.ArrayList;

/**
 * Class TDS (Symbole Table)
 */
public class TDS {


    private static int currentOffset;

    private int baseOffset;
    private ArrayList<Symbole> table;
    transient private ArrayList<TDS> children;
    transient private ArrayList<TDS> siblings;
    transient private TDS parent;

    /**
     * Default constructor
     */
    public TDS() {
        this(0);
    }

    /**
     * Constructor with offset
     * @param offset base offset in the memory stack
     */
    public TDS(int offset){
        this.table = new ArrayList<Symbole>();
        this.children = new ArrayList<TDS>();
        this.siblings = new ArrayList<TDS>();
        this.baseOffset = offset;
        this.parent = null;
    }


    /**
     * Get the size in the memory stack for a given primitive
     * @param primitive (Integer or Boolean). Keep in mind that Array isn't a primitive
     * @return size of the given primitive
     */
    private int getSizeOfPrimitive(String primitive) {
        int size;
        switch(primitive){
            case "int":
                size = SymboleInt.size;
                break;
            case "bool":
                size = SymboleBoolean.size;
                break;
            default:
                size = 0;
        }
        return size;
    }

    /**
     * Add an integer into the ST
     * @param idf for the identifier of the integer
     */
    public void addInteger(String idf) {
        SymboleInt s = new SymboleInt(idf, (currentOffset-this.baseOffset));
        this.table.add(s);
        currentOffset += SymboleInt.size;
    }

    /**
     * Add a boolean into the ST
     * @param idf for the identifier of the boolean
     */
    public void addBoolean(String idf) {
        SymboleBoolean s = new SymboleBoolean(idf, (currentOffset-this.baseOffset));
        this.table.add(s);
        currentOffset += SymboleBoolean.size;
    }

    /**
     * Add an array into the ST
     * @param idf for the identifier of the array
     * @param type is the type of value that the array is containing
     * @param shape is the shape of the array, for instance {[-3,3];[0,5]} stands for a 2D-Array with the first
     *              line going from -3 to 3 and the second one from 0 to 5
     */
    public void addArray(String idf, String type, ArrayList<int[]> shape) {
        SymboleArray s = new SymboleArray(idf, (currentOffset-this.baseOffset), type, shape);
        this.table.add(s);

        for(int[] line : shape){
            currentOffset += (((line[1] - line[0]) + 1) * getSizeOfPrimitive(type));
        }
    }

    /**
     * Add a function into the ST
     * @param idf for the identifier of the function
     * @param args contains all the arguments that the function needs
     * @param returnType is the type of value that the function will return
     * @return the ST associated to the function that you've just added
     */
    public TDS addFunction(String idf, ArrayList<Argument> args, String returnType) {
        SymboleFunction s = new SymboleFunction(idf, currentOffset, this.baseOffset, returnType, args);
        currentOffset += SymboleFunction.size;
        int offsetArgs = 0;
        Symbole s2;
        for (Argument arg : args) {
            switch(arg.getCat()){
                case "primitive":
                    switch(arg.getType()){
                        case "int":
                            offsetArgs -= SymboleInt.size;
                            s2 = new SymboleInt(arg.getIdf(), offsetArgs);
                            s.getTds().getSymboles().add(s2);
                            s2.initialize();
                            break;
                        case "bool":
                            offsetArgs -= SymboleBoolean.size;
                            s2 = new SymboleBoolean(arg.getIdf(), offsetArgs);
                            s.getTds().getSymboles().add(s2);
                            s2.initialize();
                            break;
                        default:
                    }
                    break;
                case "array":
                    String typeVal = arg.getType().split("\\[")[1].split("]")[0];
                    for(int[] line : arg.getShape()){
                        offsetArgs -= (((line[1] - line[0]) + 1) * getSizeOfPrimitive(typeVal));
                    }
                    s2 = new SymboleArray(arg.getIdf(), offsetArgs, typeVal, arg.getShape());
                    s.getTds().getSymboles().add(s2);
                    s2.initialize();
                    break;
                default:
            }
        }

        TDS funcTDS = s.getTds();

        funcTDS.getSiblings().addAll(this.children);
        this.children.add(funcTDS);
        funcTDS.setParent(this);
        this.table.add(s);

        return funcTDS;
    }

    /**
     * Get all ST created by this one
     * @return children
     */
    public ArrayList<TDS> getChildren() {
        return this.children;
    }

    /**
     * Get siblings of this ST (ST created by the parent)
     * @return siblings
     */
    public ArrayList<TDS> getSiblings() {
        return this.siblings;
    }

    /**
     * Get symbols stored in the TDS
     * @return arraylist of symbols
     */
    public ArrayList<Symbole> getSymboles() {
        return this.table;
    }

    /**
     * Get symbol by its identifier
     * @param idf identifier of the symbol you are searching for
     * @return the symbol if it's in the ST, null if it's not
     */
    public Symbole getSymboleByIDF(String idf){
        for(Symbole s : this.table){
            if(s.getIdentifier().equals(idf)) {
                return s;
            }
        }
        return null;
    }

    /**
     * Set the parent of this ST
     * @param t parent
     */
    public void setParent(TDS t) {
        this.parent = t;
    }

    /**
     * Get TDS parent of the ST
     * @return TDS parent
     */
    public TDS getParent() {
        return this.parent;
    }
}
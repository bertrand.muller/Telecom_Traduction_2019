package tds;

import java.util.ArrayList;

public class ArgumentArray extends Argument {

    public ArgumentArray(String i, String type, ArrayList<int[]> shape, boolean ref){
        super(i, "array", "array[" + type + "][" + shape.size() + "]", shape, ref);
    }

}

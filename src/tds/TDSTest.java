package tds;

import java.util.ArrayList;

public class TDSTest {

    public static void main(String[] args) {

        // Outil de visualisation JSON : http://jsonviewer.stack.hu/

        TDS tds = new TDS();

        tds.addBoolean("i");

        tds.addInteger("j");

        ArrayList<int[]> shape = new ArrayList<int[]>();
        int[] s1 = {-3,3};
        shape.add(s1);
        int[] s2 = {-3,3};
        shape.add(s2);
        tds.addArray("k", "int",shape);

        ArrayList<Argument> args2 = new ArrayList<Argument>();
        Argument a1 = new ArgumentPrimitive("a1", "int", true);
        ArrayList<int[]> shape2 = new ArrayList<int[]>();
        int[] s3 = {1,4};
        shape2.add(s3);
        Argument a2 = new ArgumentArray("a2", "bool", shape2, false);
        args2.add(a1);
        args2.add(a2);
        TDS func = tds.addFunction("maxTAB", args2, "bool");

        func.addInteger("Feuer");
        func.addBoolean("Sohne");

        System.out.println(TDSUtils.TDS2JSON(tds));
    }

}

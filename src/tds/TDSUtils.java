package tds;

import com.google.gson.Gson;

public class TDSUtils {

    /**
     * Check if the symbol is in the current scope
     * @param idf identifier of the symbol
     * @param tds current st where we are
     * @return boolean which is true if the symbol is in the tds scope, else false
     */
    public static boolean isSymbolInScope(String idf, TDS tds){
        TDS current = tds;

        while(current != null){
            if(current.getSymboleByIDF(idf) != null) {
                return true;
            }

            current = current.getParent();
        }

        return false;
    }

    /**
     * Retrieve symbol from the current scope
     * @param idf identifier of the symbol
     * @param tds current st where we are
     * @return symbol if it's present, else null
     */
    public static Symbole getSymbolInScope(String idf, TDS tds) {
        TDS current = tds;

        while(current != null){
            Symbole s = current.getSymboleByIDF(idf);
            if(s != null){
                return s;
            }

            current = current.getParent();
        }
        return null;
    }

    /**
     * Check if the symbol is in the current scope
     * @param idf identifier of the symbol
     * @param tds current st where we are
     * @return boolean which is true if the symbol is initialized, else false
     */
    public static boolean isSymbolInitialized(String idf, TDS tds) {
        Symbole s = TDSUtils.getSymbolInScope(idf, tds);

        if(s != null){
            return s.isInitialized();
        } else {
            return false;
        }
    }

    /**
     * Generate JSON String from ST Structure
     * @param tds ST to convert into JSON
     * @return JSON String
     */
    public static String TDS2JSON(TDS tds){
        Gson gson = new Gson();
        return gson.toJson(tds);
    }

}
package tds;

import java.util.ArrayList;

public class Argument {

    private String idf;
    private String cat;
    private ArrayList<int[]> shape;
    private String type;
    private boolean refmode;

    /**
     * Constructor
     * @param idf for identifier
     * @param cat for categorie (example : Array or Primitive)
     * @param type for type of argument (will determine which symbol is this argument)
     * @param shape for shape of the argument (useful for an array)
     */
    public Argument(String idf, String cat, String type, ArrayList<int[]> shape) {
        this(idf, cat, type, shape, false);
    }

    /**
     * Constructor with refmode
     * @param idf for identifier
     * @param cat for categorie (example : Array or Primitive)
     * @param type for type of argument (will determine which symbol is this argument)
     * @param shape for shape of the argument (useful for an array)
     * @param refMode true if we use the reference of the argument or its value
     */
    public Argument(String idf, String cat, String type, ArrayList<int[]> shape, boolean refMode) {
        this.idf = idf;
        this.cat = cat;
        this.shape = shape;
        this.type = type;
        this.refmode = refMode;
    }

    /**
     * Get the category of the argument
     * @return category
     */
    public String getCat(){
        return this.cat;
    }

    /**
     * Get the identifier of the argument
     * @return identifier
     */
    public String getIdf() {
        return this.idf;
    }

    /**
     * Get the shape of the argument (useful with an array)
     * @return shape
     */
    public ArrayList<int[]> getShape() {
        return this.shape;
    }

    /**
     * Get the type of argument (eg. : Integer, Boolean, ...)
     * @return type
     */
    public String getType() {
        return this.type;
    }

    /**
     * Reference mode is enable ?
     * @return true if the argument is a reference, false else
     */
    public boolean isRefmode() {
        return this.refmode;
    }

}

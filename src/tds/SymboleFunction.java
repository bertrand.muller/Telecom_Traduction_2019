package tds;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class SymboleFunction extends Symbole {

    public static int size = 1;

    private TDS tds;
    private ArrayList<Argument> args;

    /**
     * Default constructor
     * @param i identifier
     * @param o global offset of memory
     * @param bo base offset for the new ST
     * @param r return type
     */
    public SymboleFunction(String i, int o, int bo, String r, ArrayList<Argument> args){
        super(i, (o-bo), r);
        this.tds = new TDS(o+1);
        this.args = args;
    }

    /**
     * Get the TDS associated to this function
     * @return tds of this function
     */
    public TDS getTds() {
        return this.tds;
    }

    public ArrayList<Argument> getArguments() {
        return this.args;
    }

}

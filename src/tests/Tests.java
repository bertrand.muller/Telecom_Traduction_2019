package tests;

import org.junit.jupiter.api.*;

import java.io.*;

import static org.junit.jupiter.api.Assertions.fail;

public class Tests {

    public String getFileContent(String path) {
        String cExpected = "";
        try {
            InputStream is = new FileInputStream(path);
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line = br.readLine();
            StringBuilder sb = new StringBuilder();
            boolean atLeastOneLine = false;
            while(line != null)  {
                sb.append(line).append(System.getProperty("line.separator"));
                line = br.readLine();
                atLeastOneLine = true;
            }
            cExpected = sb.toString();
            if(atLeastOneLine) {
                cExpected = cExpected.substring(0, sb.toString().length()-System.getProperty("line.separator").length());
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }

        return cExpected;
    }

    public void launchTest(String leacPath, String expectedPath, String errorsPath) {
        try {
            String[] args = {leacPath};
            main.Test.main(args);
            Assertions.assertEquals(getFileContent(expectedPath), main.Test.generator.getCode());
            Assertions.assertEquals(getFileContent(errorsPath), String.join("\n", main.Test.errors));
        } catch (Exception e) {
            fail("ERROR !");
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public static String getTestName(int nb) {
        String firstLine = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader("src/examples/tests/" + nb + ".leac"));
            String fl = br.readLine();
            firstLine = fl.substring(fl.indexOf("/*") + 3, fl.indexOf("*/"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return firstLine;
    }

    @RepeatedTest(value=25, name="Test n°{currentRepetition}")
    public void test(RepetitionInfo repetitionInfo) {
        System.out.println("Test " + repetitionInfo.getCurrentRepetition() + " -> " + getTestName(repetitionInfo.getCurrentRepetition()));
        String leacPath     = "src/examples/tests/" + repetitionInfo.getCurrentRepetition() + ".leac";
        String expectedPath = "src/examples/tests/" + repetitionInfo.getCurrentRepetition() + ".c";
        String errorsPath   = "src/examples/tests/" + repetitionInfo.getCurrentRepetition() + ".errors";
        launchTest(leacPath, expectedPath, errorsPath);
    }

}
